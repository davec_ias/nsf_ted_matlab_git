clearvars;
load_consts;
test;
load_subject_data;


return
testAllAnswers = {['a. To discipline their employees when necessary;b. To reduce uncertainties for their employees, such as stock market influences;c. To build their employees’ self confidence;d. To provide education for their employees;e. To recognize and reward their employees’ achievements;f. To ensure their employees understand the dangers and competition the company faces;g. To give their employees the opportunity to try and fail']}
['a. Employees admire their leaders;b. Leaders sacrifice their comforts and tangible results so employees feel they belong;c. Leaders regularly communicate their plans to employees;d. Upper management takes smaller or no bonuses;e. Cooperation is a natural reaction among employees;f. Employees are given second chances before a layoff or demotion;g. Employees work tirelessly to contribute their talents and strengths']
{'a. Military members are more attracted to the concept of service;b. Business leaders bend rules but military leaders follow them;c. The military gets the environment “right�?;d. Better people join the military;e. Soldiers put their lives at stake because they feel their peers would do the same;f. We more readily recognize heroism in battle than elsewhere;g. The military instructs their people to trust and cooperate'}

CORRECT_ANSWERS = logical([...
	0 0 1 0 1 0 1 ... //CEG
	0 1 0 1 1 0 1 ... /BDEG
	0 0 0 1 0 0 1 ... //DG
	1 0 1 1 0 0 1 ... //ACDG
	0 0 1 0 1 0 0 ... // CE
	0 1 0 0 1 0 1 ... // BEG
]);



foo = cell(8, 37);
foo(:, 1) = C{1,2}(5:end, 1);
foo(:, 2) = C{1,3}(5:end, 1);
foo(:, 3) = {'Carol'};

C = C(1,4:37);
z = C(1,4:37);
z = [z{:}];
foo(:, 4:end) = z(5:end, :);

foo(:, 4:end) = C{1,4:37}(5:end, 1);

numLinesRead = 0;
EXPECTED_NUM_DELIMITERS = 38;
while (1==1)
    curLine = fgetl(fID);
    if (curLine == -1), break; end
    numLinesRead = numLinesRead + 1;
    numDelim = numel(strfind(curLine, '","'));
    assert(numDelim == EXPECTED_NUM_DELIMITERS, 'Invalid data on line %d of "%s"', numLinesRead, curLine);
    
    
    
    
    
end


foo = cell(8, 37);
foo(:, 1) = C{1,2}(5:end, 1);
foo(:, 2) = C{1,3}(5:end, 1);
foo(:, 3) = {'Carol'};

C = C(1,4:37);
z = C(1,4:37);
z = [z{:}];
foo(:, 4:end) = z(5:end, :);

foo(:, 4:end) = C{1,4:37}(5:end, 1);

numLinesRead = 0;
EXPECTED_NUM_DELIMITERS = 38;
while (1==1)
    curLine = fgetl(fID);
    if (curLine == -1), break; end
    numLinesRead = numLinesRead + 1;
    numDelim = numel(strfind(curLine, '","'));
    assert(numDelim == EXPECTED_NUM_DELIMITERS, 'Invalid data on line %d of "%s"', numLinesRead, curLine);
    
    
    
    
    
end

