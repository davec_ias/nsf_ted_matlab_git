TED_FILE_ROOT = 'C:\nsf_ted_matlab_data';
CSV_LINE_FORMAT_SPEC = '%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q\n';

CAROL_NUM_SKIP_LINES = 1;
CAROL_FIRST_FILENAME = fullfile(TED_FILE_ROOT, 'survey_output\carol_first.csv');

SIMON_NUM_SKIP_LINES = 1;
SIMON_FIRST_FILENAME = fullfile(TED_FILE_ROOT, 'survey_output\simon_first.csv');

NUM_COLUMNS = 37;
CORRECT_ANSWERS = logical([...
	0 0 1 0 1 0 1 ... //CEG
	0 1 0 1 1 0 1 ... /BDEG
	0 0 0 1 0 0 1 ... //DG
	1 0 1 1 0 0 1 ... //ACDG
	0 0 1 0 1 0 0 ... // CE
	0 1 0 0 1 0 1 ... // BEG
]);

NUM_ANSWERS = 42;