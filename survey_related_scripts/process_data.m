clearvars;
load_consts;
load_subject_data;
test;

binaryAnswers = logical(zeros(numSubjects, NUM_ANSWERS)); %#ok<LOGL> %matlab 2011 doesn't support direct declaration of logicals...
binaryCorrectness = logical(zeros(numSubjects, NUM_ANSWERS)); %#ok<LOGL>

for subj_i = 1:numSubjects
    binaryAnswers(subj_i, :) = GetBinaryAnswersFromSubjectRecord(subjectData(subj_i,:));
    binaryCorrectness(subj_i, :) = (binaryAnswers(subj_i, :) == CORRECT_ANSWERS);
end

numCorrectBySubj = sum(binaryCorrectness, 2);
numCorrectByAnswer = sum(binaryCorrectness, 1);

[coeffAnswers, scoreAnswers, latentAnswers] = princomp(double(binaryAnswers));
[coeffSubj, scoreSubj, latentSubj] = princomp(double(binaryAnswers)');

minByAnswer = min(numCorrectByAnswer)
maxByAnswer = max(numCorrectByAnswer)
medianByAnswer = median(numCorrectByAnswer)
meanByAnswer = mean(numCorrectByAnswer)

minBySubj= min(numCorrectBySubj)
maxBySubj = max(numCorrectBySubj)
medianBySubj = median(numCorrectBySubj)
meanBySubj = mean(numCorrectBySubj)
%hist(numCorrectByAnswer, [0:21]);

return