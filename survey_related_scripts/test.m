load('testSubjAllAnswers.mat');
load('testSubjCorrectAnswers.mat');

testSubjCorrectAnswers = GetBinaryAnswersFromSubjectRecord(testSubjCorrectAnswers);
testSubjAllAnswers = GetBinaryAnswersFromSubjectRecord(testSubjAllAnswers);

assert(sum(testSubjCorrectAnswers == CORRECT_ANSWERS) == NUM_ANSWERS);
assert(sum(testSubjAllAnswers == CORRECT_ANSWERS) == sum(CORRECT_ANSWERS));

for i = 1:numSubjects
	curID = subjectData{i, 1};
	for j = 1:numSubjects
		if (i == j), continue; end
		assert(~strcmp(curID, subjectData{j, 1}), 'Duplicate ID of "%s" detected in rows %d and %d', curID, i, j);
	end
end