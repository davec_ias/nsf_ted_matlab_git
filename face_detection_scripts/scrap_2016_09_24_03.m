
foo = insertShape(curImg, 'Rectangle', curFace);

%FACE_OUTPUT_LOCATION = 'C:\temp\shot_detection\1\carol_dweck_the_power_of_believing_that_you_can_improve_first_9_minutes_Shot_Keyframes\detected_faces';
FACE_DETECTOR_MODEL_PATH = fullfile(VGG_LOCATION, 'data/face_model.mat');

addpath(VGG_LOCATION);






faceDetector = lib.face_detector.dpmCascadeDetector(FACE_DETECTOR_MODEL_PATH);
	curImg = imread(fullfile(IMAGES_LOCATION, curFile.name));
	detectedFaces = faceDetector.detect(curImg);
	numFaces = min(size(detectedFaces, 2), numel(detectedFaces)); %numel due to catch when zero faces detected
	
fprintf('Complete. Processed %d files in %6.2f seconds.\n', numel(dirResults), toc(outerTic));


return

todo compare Face
config.paths.net_path = fullfile(VGG_LOCATION, 'data/vgg_face.mat');
convNet = lib.face_feats.convNet(config.paths.net_path);

% Adjust the net so we grab the output from final inner layer, instead of
% actual face classification from the original dataset
convNet.net.layers = convNet.net.layers(1:35);


img = imread(fullfile(VGG_LOCATION, 'ak.jpg'));
det = faceDet.detect(img);
crop = lib.face_proc.faceCrop.crop(img,det(1:4,1));
%[score,class] = max(convNet.simpleNN(crop));

feats = norm(convNet.simpleNN(crop));