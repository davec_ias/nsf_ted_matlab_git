% Dave Chisholm dave@davechisholm.net Sept 2016
% Partially based on VGG Face demo code by Omkar M. Parkhi

clearvars;

VGG_LOCATION = 'C:\lib\vgg_face_matconvnet';
IMAGES_LOCATION = 'C:\temp\shot_detection\1\carol_dweck_the_power_of_believing_that_you_can_improve_first_9_minutes_Shot_Keyframes';
FACE_OUTPUT_LOCATION = 'C:\temp\shot_detection\1\carol_dweck_the_power_of_believing_that_you_can_improve_first_9_minutes_Shot_Keyframes\detected_faces';
FACE_DETECTOR_MODEL_PATH = fullfile(VGG_LOCATION, 'data/face_model.mat');
STATUS_MESSAGE_TIME_INTERVAL_SEC = 15;
addpath(VGG_LOCATION);

faceDetector = lib.face_detector.dpmCascadeDetector(FACE_DETECTOR_MODEL_PATH);
dirResults = dir(IMAGES_LOCATION);

outerTic = tic();
innerTic = outerTic;
for i_file = 1:numel(dirResults)
	innerElapsed = toc(innerTic);
	if innerElapsed > STATUS_MESSAGE_TIME_INTERVAL_SEC
		fprintf('Processed %d of %d files, total %6.2f seconds elapsed...\n', i_file, numel(dirResults), toc(outerTic))
		innerTic = tic();
	end
	curFile = dirResults(i_file);
	[~,curNameNoExtension, extension] = fileparts(curFile.name);
	if ~(strcmpi(extension, '.png') || strcmpi(extension, '.jpg') || strcmpi(extension, '.jpeg')), continue; end
	curImg = imread(fullfile(IMAGES_LOCATION, curFile.name));
	detectedFaces = faceDetector.detect(curImg);
	numFaces = min(size(detectedFaces, 2), numel(detectedFaces)); %numel due to catch when zero faces detected
	for j_face = 1:numFaces
		curCrop = lib.face_proc.faceCrop.crop(curImg,detectedFaces(1:4,j_face));
		curFaceOutputFile = fullfile(FACE_OUTPUT_LOCATION, sprintf('%s.face%03d.png', curNameNoExtension, j_face));
		imwrite(curCrop, curFaceOutputFile);
	end
end
fprintf('Complete. Processed %d files in %6.2f seconds.\n', numel(dirResults), toc(outerTic));


return

todo compare Face = vision.CascadeObjectDetector;
config.paths.net_path = fullfile(VGG_LOCATION, 'data/vgg_face.mat');
convNet = lib.face_feats.convNet(config.paths.net_path);

% Adjust the net so we grab the output from final inner layer, instead of
% actual face classification from the original dataset
convNet.net.layers = convNet.net.layers(1:35);


img = imread(fullfile(VGG_LOCATION, 'ak.jpg'));
det = faceDet.detect(img);
crop = lib.face_proc.faceCrop.crop(img,det(1:4,1));
%[score,class] = max(convNet.simpleNN(crop));

feats = norm(convNet.simpleNN(crop));