% Dave Chisholm dave@davechisholm.net Sept 2016
% Partially based on VGG Face demo code by Omkar M. Parkhi

clearvars;

IMAGES_LOCATION = 'C:\temp\shot_detection\1\carol_dweck_the_power_of_believing_that_you_can_improve_first_9_minutes_Shot_Keyframes';
FACE_OUTPUT_LOCATION = 'C:\temp\shot_detection\1\carol_dweck_the_power_of_believing_that_you_can_improve_first_9_minutes_Shot_Keyframes\matlab_builtin_face_cascade_detector\raw_output';
STATUS_MESSAGE_TIME_INTERVAL_SEC = 15;


faceDetector = vision.CascadeObjectDetector;
dirResults = dir(IMAGES_LOCATION);

outerTic = tic();
innerTic = outerTic;
for i_file = 1:numel(dirResults)
	innerElapsed = toc(innerTic);
	if innerElapsed > STATUS_MESSAGE_TIME_INTERVAL_SEC
		fprintf('Processed %d of %d files, total %6.2f seconds elapsed...\n', i_file, numel(dirResults), toc(outerTic))
		innerTic = tic();
	end
	curFile = dirResults(i_file);
	[~,curNameNoExtension, extension] = fileparts(curFile.name);
	if ~(strcmpi(extension, '.png') || strcmpi(extension, '.jpg') || strcmpi(extension, '.jpeg')), continue; end
	curImg = imread(fullfile(IMAGES_LOCATION, curFile.name));
	detectedFaces = step(faceDetector,curImg);
	numFaces = size(detectedFaces, 1);
	
	for j_face = 1:numFaces
		curFace = detectedFaces(j_face, :);
		x = curFace(1); y = curFace(2); w = curFace(3); h = curFace(4);
		curCrop = curImg(y:y+h-1, x:x+w-1, :);
		%imshow(curCrop);
		curFaceOutputFile = fullfile(FACE_OUTPUT_LOCATION, sprintf('%s.face%03d.png', curNameNoExtension, j_face));
		imwrite(curCrop, curFaceOutputFile);
	end
end

return
