%ID, GENDER, AGE, SHOWN FIRST, 
global FIXATION_FILE_LOCATION glbSurveyHeader glbSurveyRawResponses glbValidSubjects VERBOSE_STATUS_MESSAGES KNOWN_INVALID_SUBJECTS;

IDX_SURVEY_ID = 2;
IDX_SURVEY_GENDER = 4;
IDX_SURVEY_AGE = 5;
IDX_SURVEY_SHOWN_FIRST = 38;

glbValidSubjects = {'ID', 'Gender', 'Age', 'VideoShownFirst', ...
	'CarolFixations', 'CarolMeanFixationDuration', 'CarolStdDevFixationDuration', 'CarolNumFixations', 'CarolTotalTimeSpentFixating'...
	'SimonFixations', 'SimonMeanFixationDuration', 'SimonStdDevFixationDuration', 'SimonNumFixations', 'SimonTotalTimeSpentFixating'};
if VERBOSE_STATUS_MESSAGES, fprintf('Loading subjects... '); end
for i = 1:size(glbSurveyRawResponses, 1)
	curID = lower(glbSurveyRawResponses{i, IDX_SURVEY_ID});
	curGender = glbSurveyRawResponses{i, IDX_SURVEY_GENDER};
	curAge = glbSurveyRawResponses{i, IDX_SURVEY_AGE};
	curShownFirst = glbSurveyRawResponses{i, IDX_SURVEY_SHOWN_FIRST};
	if VERBOSE_STATUS_MESSAGES
		fprintf('%s(%d of %d)... ', curID, i,size(glbSurveyRawResponses, 1)); 
		if (mod(i, 4) == 0), fprintf('\n'); end
	end
		
	
	carolFixationFileLoc = FindFixationFile(FIXATION_FILE_LOCATION, curID, 'carol');
	simonFixationFileLoc = FindFixationFile(FIXATION_FILE_LOCATION, curID, 'simon');

		
	if isempty(carolFixationFileLoc) || isempty(simonFixationFileLoc)
		if isempty(strfind(KNOWN_INVALID_SUBJECTS, curID))
			if isempty(carolFixationFileLoc), error('Could not fix carol fixation file for %s', curID); end
			if isempty(simonFixationFileLoc), error('Could not fix simon fixation file for %s', curID); end
			if VERBOSE_STATUS_MESSAGES, fprintf('WARNING: skipped %s due to missing fixation file(s)... ', curID); end
		end
		continue;
	end
	glbValidSubjects(end+1, :) = {curID, curGender, str2double(curAge), curShownFirst, [], -1, -1, -1, -1, [], -1, -1, -1, -1}; %#ok<SAGROW>
	
	[glbValidSubjects{end, 5}, glbValidSubjects{end, 6}, glbValidSubjects{end, 7}, glbValidSubjects{end, 8}, glbValidSubjects{end, 9}] = ...
		GetDataFromFixationFile(carolFixationFileLoc);
	[glbValidSubjects{end, 10}, glbValidSubjects{end, 11}, glbValidSubjects{end, 12}, glbValidSubjects{end, 13}, glbValidSubjects{end, 14}] = ...
		GetDataFromFixationFile(simonFixationFileLoc);
	
end
if VERBOSE_STATUS_MESSAGES, fprintf('\nSubject loading complete!\n'); end

clear i curID curGender curAge curShownFirst carolFixationFileLoc simonFixationFileLoc IDX_SURVEY_ID IDX_SURVEY_GENDER IDX_SURVEY_AGE IDX_SURVEY_SHOWN_FIRST;