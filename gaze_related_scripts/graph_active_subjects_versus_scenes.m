%2016-09-21 davec@cs.columbia.edu
YMAX_TO_USE = 15;
YMIN_TO_USE = 0;

%windowd or non...
%dataToUse = windowedAvgs; %windowed
dataToUse = activeSubjectCounts; %no window



figure; set(gcf,'color','w');
yLabelText = sprintf('Num. Subjects Currently Fixated (of %d subjects)', numSubj);
xLabelText = 'Time (seconds)';
yMin = YMIN_TO_USE; yMax = YMAX_TO_USE;

stairs(dataToUse(:,1) / 1000, dataToUse(:,2), 'LineWidth', 1);
title(sprintf('Number of subjects actively fixating over time'));
ylim([yMin yMax]); ylabel(yLabelText); xlabel(xLabelText);

hold on;

%todo fix this hack-y way of controlling axes when showing shot boundaries
curYL = ylim; curYMin = curYL(1);  curYMax = curYL(2);
curXL = xlim; curXMin = curXL(1);  curXMax = curXL(2);
newYL = [curYMin - 1, curYMax]; ylim(newYL);

for i = 1:numel(shotStartFrameNums)
	curStartTime = shotStartFrameNums(i)/FPS_OLD;
	if i < numel(shotStartFrameNums)
		curEndTime = shotStartFrameNums(i+1) / FPS_OLD;
	else
		curEndTime = max(allEnds) / 1000;
	end
	colorToUse = ShotTypeIDToColorRGB(shotTypesIDNums(i));
	rectangle('Position', [curStartTime, curYMin - 1, curEndTime - curStartTime, 1], 'FaceColor', colorToUse);
	line([curStartTime curStartTime], newYL, 'Color', 'black', 'LineStyle', ':');
end


hold on;
h = zeros(9, 1);
textLabels = {};
for i = 0:8
	h(i+1) = plot(-100,-100,'s', 'visible', 'on', 'color', ShotTypeIDToColorRGB(i), 'MarkerFaceColor', ShotTypeIDToColorRGB(i));
	textLabels{i+1} = ShotTypeIDToDescriptionString(i); %#ok<SAGROW>
end
legend(h, textLabels);