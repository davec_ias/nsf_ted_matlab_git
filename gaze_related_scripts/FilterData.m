%assumes aData in format of one row per fixation, each row as:
%[FixationStartInMS FixationDurationInMS FixationEndInMS XPosInPixels YPosInPixels]
function [filteredData] = FilterData(aData, aDurationMinThresholdMS, ...
		aDurationMaxThresholdMS, aMinX, aMinY, aMaxX, aMaxY)

matchingRowIndices = ( ...
	((aData(:, 2) >= aDurationMinThresholdMS) | (aDurationMinThresholdMS == -1)) & ...
	((aData(:, 2) <= aDurationMaxThresholdMS) | (aDurationMaxThresholdMS == -1)) & ...
	((aData(:, 4) >= aMinX) | (aMinX == -1)) & ...
	((aData(:, 5) >= aMinY) | (aMinY == -1)) & ...
	((aData(:, 4) <= aMaxX) | (aMaxX == -1)) & ...
	((aData(:, 5) <= aMaxY) | (aMaxY == -1))...
);

filteredData = aData(matchingRowIndices, :);

