%sept 30 2016
%dave@davechisholm.net

%% consts which are specific to a given hostname where code is being run
global HOSTNAME;
	[~, HOSTNAME] = system('hostname'); HOSTNAME = strtrim(HOSTNAME);

%Set some computer specific consts
global DATA_ROOT_DIRECTORY;
DATA_ROOT_DIRECTORY = 'C:\nsf_ted_matlab_data';


%% consts which you may wish to adjust per run to affect behavior
global FORCE_DATA_RELOAD 
FORCE_DATA_RELOAD = false;

%if true, will only use fixations within the x, y, min and max duration
%bounds that are specified further down
global ENABLE_FILTERING_FIXATIONS;
ENABLE_FILTERING_FIXATIONS = false;

global VERBOSE_STATUS_MESSAGES;
VERBOSE_STATUS_MESSAGES = true;

%% Derive subpaths from root

global FIXATION_FILE_LOCATION;
	FIXATION_FILE_LOCATION = fullfile(DATA_ROOT_DIRECTORY, 'fixations');
global SURVEY_ANSWERS_LOCATION;
	SURVEY_ANSWERS_LOCATION = fullfile(DATA_ROOT_DIRECTORY, 'survey_output');
global VIDEO_SHOT_ANNOTATIONS_LOCATION;
	VIDEO_SHOT_ANNOTATIONS_LOCATION = fullfile(DATA_ROOT_DIRECTORY, 'video_shot_annotations');
global VIDEO_GESTURE_ANNOTATIONS_LOCATION;
	VIDEO_GESTURE_ANNOTATIONS_LOCATION = fullfile(DATA_ROOT_DIRECTORY, 'gesture_annotations');
global VIDEO_FILES_LOCATION;
	VIDEO_FILES_LOCATION = fullfile(DATA_ROOT_DIRECTORY, 'video_files');



%% consts which typically won't change

global GLOBALS;
GLOBALS.SURVEY.CORRECT_ANSWERS = logical([...
	0 0 1 0 1 0 1 ... //CEG
	0 1 0 1 1 0 1 ... /BDEG
	0 0 0 1 0 0 1 ... //DG
	1 0 1 1 0 0 1 ... //ACDG
	0 0 1 0 1 0 0 ... // CE
	0 1 0 0 1 0 1 ... // BEG
]);

GLOBALS.SURVEY.NUM_ANSWERS = 42;
GLOBALS.SURVEY.NUM_CAROL_QUESTIONS = 21;

global glbVideoInfos;
glbVideoInfos = {
	'STR_ID', 'FILENAME', 'VIDEO_READER_OBJ', 'SHOT_ANNOTATIONS', 'GESTURE_ANNOTATIONS', 'FPS';
	'simon',  fullfile(VIDEO_FILES_LOCATION, 'simon.mp4'), 'NOT_LOADED', 'NOT_LOADED', 'NOT_LOADED', 24;
	'carol', fullfile(VIDEO_FILES_LOCATION, 'carol.mp4'), 'NOT_LOADED', 'NOT_LOADED', 'NOT_LOADED', 25;
};
global	IDX_VIDEO_STR_ID; 
		IDX_VIDEO_STR_ID = 1;
global	IDX_VIDEO_FILENAME; 
		IDX_VIDEO_FILENAME = 2;
global	IDX_VIDEO_READER; 
		IDX_VIDEO_READER = 3;
global	IDX_VIDEO_SHOT_ANNO; 
		IDX_VIDEO_SHOT_ANNO = 4;
global	IDX_VIDEO_GESTURE_ANNO; 
		IDX_VIDEO_GESTURE_ANNO = 5;
global	IDX_VIDEO_FPS; 
		IDX_VIDEO_FPS = 6;
		
global	SIMON_IDX;
		SIMON_IDX = 2;
global	CAROL_IDX;
		CAROL_IDX = 3;



global NINE_DISTINCT_COLORS;
NINE_DISTINCT_COLORS = {
	[255 000 000];
	[000 255 000];
	[000 000 255];
	[000 255 255];
	[255 000 255];
	[128 128 255];
	[128 128 128];
	[000 000 128];
	[255 128 000];
};


global SHOT_TYPES;
SHOT_TYPES = {
	'ID_NUM', 'DESCRIPTION', 'RGB_TRIPLET';
	0, 'Intro/Outro', NINE_DISTINCT_COLORS{1};
	1, 'Head-On Speaker', NINE_DISTINCT_COLORS{2};
	2, 'Side/45 Speaker', NINE_DISTINCT_COLORS{3};
	3, 'Audience', NINE_DISTINCT_COLORS{4};
	4, 'Slides/Graphics', NINE_DISTINCT_COLORS{5};
	5, 'Speaker From Distance', NINE_DISTINCT_COLORS{6};
	6, 'Facial Close-up', NINE_DISTINCT_COLORS{7};
	7, 'Onstage Demo', NINE_DISTINCT_COLORS{8};
	8, 'Other', NINE_DISTINCT_COLORS{9};
};

%offset by due, due to header col, and matlab one based indexing
global	SHOT_TYPES_ID_OFFSET;
		SHOT_TYPES_ID_OFFSET = 2;
%readable column number IDs 
global	IDX_SHOT_TYPE_ID_NUM; 
		IDX_SHOT_TYPE_ID_NUM = 1;
global	IDX_SHOT_TYPE_DESCRIPTION; 
		IDX_SHOT_TYPE_DESCRIPTION = 2;
global	IDX_SHOT_TYPE_RGB_TRIPLET; 
		IDX_SHOT_TYPE_RGB_TRIPLET = 3;
		

% all videos are assumed to be 25 frames per second
%global FPS;
%FPS = 25;

%% globals related to screen settings of gaze monitoring hardware setup
global	GAZE_SCREEN_WIDTH_PX;
		GAZE_SCREEN_WIDTH_PX = 1024;
global	GAZE_SCREEN_HEIGHT_PX;
		GAZE_SCREEN_HEIGHT_PX = 768;
global	ORIGINAL_VIDEO_WIDTH_PX;
		ORIGINAL_VIDEO_WIDTH_PX = 854;
global	ORIGINAL_VIDEO_HEIGHT_PX;
		ORIGINAL_VIDEO_HEIGHT_PX = 480;

global	KNOWN_INVALID_SUBJECTS;
		%KNOWN_INVALID_SUBJECTS = lower('LV1 MCY1 CA1 klt1 mm1 JHH1 SD2');
		KNOWN_INVALID_SUBJECTS = lower('MCY1');
		

%% some consts related to older scripts. I'm slowly phasing these out...
global SHOT_ANNOTATION_LOCATION_OLD
global	VIDEO_TO_USE_OLD;
		VIDEO_TO_USE_OLD = 'carol';
		%VIDEO_TO_USE_OLD = 'simon';
if strcmp(VIDEO_TO_USE_OLD, 'carol') 
	SHOT_ANNOTATION_LOCATION_OLD = fullfile(DATA_ROOT_DIRECTORY, 'video_shot_annotations', 'carol.shot_annotation');
	FPS_OLD = 25;
elseif strcmp(VIDEO_TO_USE_OLD, 'simon') 
	SHOT_ANNOTATION_LOCATION_OLD = fullfile(DATA_ROOT_DIRECTORY, 'video_shot_annotations', 'simon.shot_annotation');
	FPS_OLD = 24;
else
	error('Unknown VIDEO_TO_USE of "%s"', VIDEO_TO_USE_OLD);
end


ENABLE_FILTERING = true;

DO_ACTIVE_FIXATIONS = true;
DO_WINDOWED_FIXATION_COUNTS = true;
DO_HISTOGRAM_OF_FIXATION_DURATION = true;
DO_DURATION_SCATTER_PLOTS = true;

WINDOW_WIDTH_MS_ACTIVE_FIXATIONS = 1000;
WINDOW_SHIFT_MS_ACTIVE_FIXATIONS = 500;

WINDOW_WIDTH_MS_WINDOWED_COUNTS = 5000;
WINDOW_SHIFT_MS_WINDOWED_COUNTS = 5000;


%all X and Y measured in pixels...
%to disable removal of "off screen" fixations, set these to -1
if ENABLE_FILTERING
	FIXATION_DURATION_MIN_THRESHOLD_MS = 1000;
	FIXATION_DURATION_MAX_THRESHOLD_MS = 2000;
	
	FIXATION_MIN_X = (GAZE_SCREEN_WIDTH_PX - ORIGINAL_VIDEO_WIDTH_PX) / 2;
	FIXATION_MIN_Y = (GAZE_SCREEN_HEIGHT_PX - ORIGINAL_VIDEO_HEIGHT_PX) / 2;

	FIXATION_MAX_X = FIXATION_MIN_X + ORIGINAL_VIDEO_WIDTH_PX;
	FIXATION_MAX_Y = FIXATION_MIN_Y + ORIGINAL_VIDEO_HEIGHT_PX;

else
	FIXATION_DURATION_MIN_THRESHOLD_MS = -1;
	FIXATION_DURATION_MAX_THRESHOLD_MS = -1;
	FIXATION_MIN_X = -1;
	FIXATION_MIN_Y = -1;
	FIXATION_MAX_X = -1;
	FIXATION_MAX_Y = -1;
end
	
%[FixationStartInMS FixationDurationInMS FixationEndInMS XPosInPixels YPosInPixels]
IDX_FIX_START = 1;
IDX_FIX_DUR = 2;
IDX_FIX_END = 3;
IDX_FIX_X = 4;
IDX_FIX_Y = 5;



global NUM_SHOT_TYPES;
NUM_SHOT_TYPES = size(SHOT_TYPES, 1) - 1;

return

%% depricated, left here for reference


%[FixationStartInMS FixationDurationInMS FixationEndInMS XPosInPixels YPosInPixels]
IDX_FIX_START = 1;
IDX_FIX_DUR = 2;
IDX_FIX_END = 3;
IDX_FIX_X = 4;
IDX_FIX_Y = 5;





FIXATIONS_LOCATION_ROOT = fullfile('C:\davechisholm_google_drive\ted_project\fixations', VIDEO_TO_USE);


if strcmp(VIDEO_TO_USE, 'carol') 
	SHOT_ANNOTATION_LOCATION = 'C:\davechisholm_google_drive\ted_project\first_four_videos_annotations\carol_dweck_the_power_of_believing_that_you_can_improve.20160330_021338.annotation';
elseif strcmp(VIDEO_TO_USE, 'simon') 
	SHOT_ANNOTATION_LOCATION = 'C:\davechisholm_google_drive\ted_project\first_four_videos_annotations\simon_sinek_why_good_leaders_make_you_feel_safe.20160330_024328.annotation';
else
	error('Unknown VIDEO_TO_USE of "%s"', VIDEO_TO_USE);
end


		
	
