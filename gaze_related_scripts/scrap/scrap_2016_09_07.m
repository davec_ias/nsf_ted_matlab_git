clearvars;

FIXATIONS_LOCATION_ROOT = 'C:\davechisholm_google_drive\ted_project\fixations';
VIDEO_TO_PROCESS = 'carol';


temp_path = 'C:\davechisholm_google_drive\ted_project\fixations\carol\an1_carol_fixation_an1_c.xls';


fid = fopen(temp_path);
headerLine = fgetl(fid);
nextLine = fgetl(fid);
fclose(fid);

assert(ischar(headerLine), 'Malformed file at %s', temp_path);
timeCol = -1; durationCol = -1; xCol = -1; yCol = -1;

colNames = strsplit(headerLine, '\t');

numCols = numel(colNames);

formatString = '';
formatString2 = '';
for i = 1:numCols
	curColName = colNames{i};
	if strcmp(curColName, 'CURRENT_FIX_START')
		timeCol = i;
		formatString = [formatString '%d']; %#ok<AGROW>
	elseif strcmp(curColName, 'CURRENT_FIX_DURATION')
		durationCol = i;
		formatString = [formatString '%d']; %#ok<AGROW>
	elseif strcmp(curColName, 'CURRENT_FIX_X')
		xCol = i;
		formatString = [formatString '%f']; %#ok<AGROW>
	elseif strcmp(curColName, 'CURRENT_FIX_Y')
		yCol = i;
		formatString = [formatString '%f']; %#ok<AGROW>
	else
		formatString = [formatString '%s']; %#ok<AGROW>
	end
	formatString2 = [formatString2 '%s']; %#ok<AGROW>
end



formatString = [formatString '\n']; %#ok<AGROW>
formatString2 = [formatString2 '\n']; %#ok<AGROW>

assert(((timeCol ~= -1) && (durationCol ~= -1) && (xCol ~= -1) && (yCol ~= -1)), 'Could not read all column names from header');

%fid = fopen(temp_path);

%cols = textscan(fid, formatString2, 'HeaderLines', 1);%, 'Delimiter' , '\r\n');
cols = textscan(nextLine, formatString2);
fclose(fid);
