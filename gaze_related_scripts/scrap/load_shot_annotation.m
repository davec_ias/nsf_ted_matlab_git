%2016-09-20 davec@cs.columbia.edu
SHOT_ANNOTATION_LOCATION = 'C:\davechisholm_google_drive\ted_project\first_four_videos_annotations\carol_dweck_the_power_of_believing_that_you_can_improve.20160330_021338.annotation';


fileID = fopen(SHOT_ANNOTATION_LOCATION);
shotAnnotations = textscan(fileID,'%d%d%s%s', 'HeaderLines', 1, 'Delimiter', ',');
shotAnnotationFrameNums = shotAnnotations{1};
annotatedShotFrameNums = shotAnnotationFrameNums(shotAnnotationFrameNums < automaticEndTime);
fclose(fileID);
