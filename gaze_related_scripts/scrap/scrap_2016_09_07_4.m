WINDOW_WIDTH_MS = 50000;
WINDOW_SHIFT_MS = 10000;

endOfVideoMS = max(fixationCounts(:, 1));

windowStartMS = 0;
windowedAvgs = [];
while (true)
	windowEndMS = windowStartMS + WINDOW_WIDTH_MS;
	windowCenterMS = (windowStartMS + windowEndMS) / 2;
	if windowEndMS > endOfVideoMS, break; end
	
	indicesOfRelevantPoints = (fixationCounts(:,1) >= windowStartMS) & (fixationCounts(:,1) < windowEndMS);
	relevantPoints = fixationCounts(indicesOfRelevantPoints, :);

	%numRelevantPoints2 = size(relevantPoints, 1);
	%avgValue2 = sum(relevantPoints(:, 2)) / numRelevantPoints;
	
	weightedValues = relevantPoints(:,2) .* relevantPoints(:,3);
	totalWeight = sum(relevantPoints(:,3));
	
	
	avgValue = sum(weightedValues) / totalWeight;
	
	windowedAvgs = [windowedAvgs; windowCenterMS avgValue]; %#ok<AGROW>
	windowStartMS = windowStartMS + WINDOW_SHIFT_MS;
end

figure;
%plot(windowedAvgs(:,1) / 1000, windowedAvgs(:,2));
stairs(windowedAvgs(:,1) / 1000, windowedAvgs(:,2));
ylim([15 30]);

return

%% scrap

tempStarts = [10; 10; 10; 10; 35];
tempEnds = [20; 20; 30; 40; 42];

tempUniques = sortrows(unique([tempStarts; tempEnds]));

tempUniques = sortrows(unique([tempStarts; tempEnds]));

