for vid_i = 2:3
	curVidName = glbVideoInfos{vid_i, 1};
	assert(strcmpi(curVidName,  glb.rawGazeSampleCell{vid_i, 1}));
	curVidShotStartFrames = glbVideoInfos{vid_i, 4}(1, 1);
	curFPS = glbVideoInfos{vid_i, 6};
	
	curVidEigs = glb.rawGazeSampleCell{vid_i, 6};
	curVidMean = glb.rawGazeSampleCell{vid_i, 8};
	curLenMS = glb.rawGazeSampleCell{vid_i, 2};
	time = [0:1:curLenMS-1];
	
	figure; plot(time, curVidEigs(1,:));
	%figure; plot(time, curVidMean);
	yLims = ylim;
	for shot_i = 1:numel(curVidShotStartFrames)
		curX = curVidShotStartFrames{shot_i} *1000 / curFPS;
		line([curX, curX], yLims, 'color', 'black');
	end
	
	
end



return
%from google, a quick way to look for linearly dependent rows
[c,d]=lu(foo);
c(find(c~=0))=1;
column_sum=sum(c);
check_columns=find(column_sum>1);
for m=1:length(check_columns), dependentrows=find(c(:,check_columns(m))>0); end