FORCE_RELOAD = false;
if ((~(exist('glbValidSubjects', 'var') == 1)) || FORCE_RELOAD)
	load_data_from_disk;
end
[surveyStats, surveyOutput] = GetSurveyStats();

numSubjects = size(glbValidSubjects, 1) - 1;

carolScores = zeros(numSubjects, 1);
simonScores = zeros(numSubjects, 1);
allScores = zeros(numSubjects, 1);
carolMeanDurs = zeros(numSubjects, 1);
simonMeanDurs = zeros(numSubjects, 1);
allMeanDurs = zeros(numSubjects, 1);



for subj_i = 1:numSubjects
	curName = glbValidSubjects{subj_i+1, 1};
	curCarolDur = glbValidSubjects{subj_i+1, 6};
	curCarolFixCount = glbValidSubjects{subj_i+1, 8};
	curSimonDur = glbValidSubjects{subj_i+1, 11};
	curSimonFixCount = glbValidSubjects{subj_i+1, 13};
	curBothMeanDur = (curCarolDur * curCarolFixCount + curSimonDur * curSimonFixCount) / ((curCarolFixCount + curSimonFixCount));

	[totalScore, carolScore, simonScore] = GetScoresByName(surveyStats, surveyOutput, curName);
	
	allScores(subj_i) = totalScore;
	carolScores(subj_i) = carolScore;
	simonScores(subj_i) = simonScore;
	carolMeanDurs(subj_i) =curCarolDur;
	simonMeanDurs(subj_i) = curSimonDur;
	allMeanDurs(subj_i) = curBothMeanDur;
	
end

dx = 0.1; dy = 0.1;

x = carolMeanDurs; y = carolScores; titleText = 'Carol scores vs mean duration';
figure; scatter(x, y,'MarkerFaceColor', 'b'); title(titleText);
text(x+dx, y+dy, glbValidSubjects(2:end, 1)); xlabel('Mean fixation duration (ms)'); ylabel('Number correct');

x = simonMeanDurs; y = simonScores; titleText = 'Simon scores vs mean duration';
figure; scatter(x, y,'MarkerFaceColor', 'b'); title(titleText);
text(x+dx, y+dy, glbValidSubjects(2:end, 1)); xlabel('Mean fixation duration (ms)'); ylabel('Number correct');

x = allMeanDurs; y = allScores; titleText = 'Total scores vs mean duration';
figure; scatter(x, y,'MarkerFaceColor', 'b'); title(titleText); xlabel('Mean fixation duration (ms)'); ylabel('Number correct');
text(x+dx, y+dy, glbValidSubjects(2:end, 1));