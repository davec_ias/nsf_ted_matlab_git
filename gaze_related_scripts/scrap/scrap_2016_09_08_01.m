histLabels = 0:numSubj;
histCounts = zeros(1, numSubj+1); %off by 1 via matlab one-based array indexing

numPoints = size(fixationCounts, 1);

for i = 1:numPoints
	curCount = fixationCounts(i, 2) + 1; %adjust for one-based array index
	curDurationMS = fixationCounts(i, 3);
	histCounts(curCount) = histCounts(curCount) + curDurationMS;
end

return

figure;
pd = fitdist(fixationCountsByMS, 'HalfNormal');
y = pdf(pd, histLabels);
plot(histLabels,y,'LineWidth',2);

figure;
pd = fitdist(fixationCountsByMS, 'Poisson');
y = pdf(pd, histLabels);
plot(histLabels,y,'LineWidth',2);