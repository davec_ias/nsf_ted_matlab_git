clearvars;
FIXATIONS_LOCATION_ROOT = 'C:\davechisholm_google_drive\ted_project\fixations\carol\';

dirResults = dir(fullfile(FIXATIONS_LOCATION_ROOT, '*.xls'));
numSubj = numel(dirResults);
subjectData = cell(numSubj, 1);

allFixations = [];
for i = 1:numSubj
	curFile = fullfile(FIXATIONS_LOCATION_ROOT, dirResults(i).name);
	fprintf('Processing "%s"\n', curFile);
	curData = GetDataFromFixationFile(curFile);
	subjectData{i} = curData;
	allFixations = [allFixations; curData(:,[1 3])]; %#ok<AGROW>
end

return

%% scrap

temp_path = 'C:\davechisholm_google_drive\ted_project\fixations\carol\an1_carol_fixation_an1_c.xls';
subjectData = GetDataFromFixationFile(temp_path);


fid = fopen(temp_path);
headerLine = fgetl(fid);
%fclose(fid);

assert(ischar(headerLine), 'Malformed file at %s', temp_path);
timeCol = -1; durationCol = -1; xCol = -1; yCol = -1;

colNames = strsplit(headerLine, '\t');
numCols = numel(colNames);

for i = 1:numCols
	curColName = colNames{i};
	if strcmp(curColName, 'CURRENT_FIX_START')
		timeCol = i;
	elseif strcmp(curColName, 'CURRENT_FIX_DURATION')
		durationCol = i;
	elseif strcmp(curColName, 'CURRENT_FIX_X')
		xCol = i;
	elseif strcmp(curColName, 'CURRENT_FIX_Y')
		yCol = i;
	end
end



%while using textscan() would be more efficient, it seems to choke on
%either the EOL conventions of presence of literal strings '[]' in the
%content, and I have no time to keep futzing with it - davec
while (true)
	curLine = fgetl(fid);	
	if ~ischar(curLine), break; end
	colVals = strsplit(curLine, '\t');
	%format is [startTime durationMS endTime x y]. note endTime is
	%constructed from first two cols, and is thus for convenience
	curLineData = [str2double(colVals(timeCol)) str2double(colVals(durationCol)) (str2double(colVals(timeCol)) + str2double(colVals(durationCol))) str2double(colVals(xCol)) str2double(colVals(yCol))];
	subjectData = [subjectData ; curLineData]; %#ok<AGROW>
end
fclose(fid);


%sanity checks on data
%all durations positive
assert(all((subjectData(:, 2) > 0)));
%first start time non neg, each start time is later than previous
assert((subjectData(1, 1) >= 0) && all(subjectData(2:end, 1) > subjectData(1:end-1,1)));
%each start time later than previous end time
assert(all(subjectData(2:end, 1) > (subjectData(1:end-1,3))));
