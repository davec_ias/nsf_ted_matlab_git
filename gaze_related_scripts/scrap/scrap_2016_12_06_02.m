asdasdrawGazeSampleCell = {
	'VideoName', 'NumSafeSamples', 'SubjectNames', 'RawSamplesStruct', 'ZScorePupilDataMatrix', 'PCACoeff', 'PCAExplained', 'PCAMean';
	'simon', 718900, {}, {}, [], [], [], [];
	'carol', 624600, {}, {}, [], [], [], [];
};

for vid_i = 2:size(glb.rawGazeSampleCell, 1)
	curVidName = glb.rawGazeSampleCell{vid_i, 1};
	curVidSubjNames = glb.rawGazeSampleCell{vid_i, 3};
	curVidRawSamples = glb.rawGazeSampleCell{vid_i, 4};
	curLenMS = glb.rawGazeSampleCell{vid_i, 2};
	time = [0:1:curLenMS-1];
	
	
	%extract the actual gaze samples into a matrix format suitable for PCA
	curNumSubjects = size(glb.rawGazeSampleCell{vid_i, 3}, 1);
	for subj_j = 1:curNumSubjects
		figure; plot(time, curVidRawSamples{subj_j}(1:curLenMS));
		%myMeans(subj_i) rawGazeSampleCell{vid_i, 5}(subj_j, :) = rawGazeSampleCell{vid_i, 4}{subj_j}(1:curVidNumSamples)';
	end
end