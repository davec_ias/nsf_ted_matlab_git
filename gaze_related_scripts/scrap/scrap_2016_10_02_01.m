
	curVideoFileName = glbVideoInfos{i, IDX_VIDEO_FILENAME};
	curReader = VideoReader(curVideoFileName); %#ok<TNMLP>
	
	if ((curReader.Height ~= ORIGINAL_VIDEO_HEIGHT_PX) || (curReader.Width ~= ORIGINAL_VIDEO_WIDTH_PX))
		warning('Video %s has unexpected dim (%d x %d), expected (%d x %d)', ...
				curFilecurVideoFileNameName, curReader.Height, curReader.Width, ORIGINAL_VIDEO_HEIGHT_PX, ORIGINAL_VIDEO_WIDTH_PX);
	end
	
	curShotAnnoCell = {NaN, NaN};
	curShotAnnoFileName = fullfile(VIDEO_SHOT_ANNOTATIONS_LOCATION, [curID '.shot_annotation']);
	[curShotAnnoCell{1}, curShotAnnoCell{2}, ~, ~] = ReadShotAnnotationFile(curShotAnnoFileName);
	
	
	curGestureAnnoFileName = fullfile(VIDEO_GESTURE_ANNOTATIONS_LOCATION, [curID '.gesture_annotation']);
	curGestureAnnoCell = ReadGestureAnnotationFile(curGestureAnnoFileName);
	
	glbVideoInfos{i, IDX_VIDEO_READER} = curReader; %#ok<SAGROW>
	glbVideoInfos{i, IDX_VIDEO_SHOT_ANNO} = curShotAnnoCell; %#ok<SAGROW>
	glbVideoInfos{i, IDX_VIDEO_GESTURE_ANNO} = curGestureAnnoCell; %#ok<SAGROW>
	
end

clear i curID curVideoFileName curReader curShotAnnoFileName curShotAnnoCell curGestureAnnoFileName curGestureAnnoCell;

fID = fopen(CAROL_FIRST_FILENAME, 'r');
assert(fID ~= -1, 'Unable to open file for reading "%s"', CAROL_FIRST_FILENAME);
carolFirstCells = textscan(fID, CSV_LINE_FORMAT_SPEC, 'Delimiter',',');
fclose(fID);

fID = fopen(SIMON_FIRST_FILENAME, 'r');
assert(fID ~= -1, 'Unable to open file for reading "%s"', SIMON_FIRST_FILENAME);
simonFirstCells = textscan(fID, CSV_LINE_FORMAT_SPEC, 'Delimiter',',');
fclose(fID);

numCarolFirstSubjects = size(carolFirstCells{2}, 1) - CAROL_NUM_SKIP_LINES;
numSimonFirstSubjects = size(simonFirstCells{2}, 1) - SIMON_NUM_SKIP_LINES;

numSubjects = numSimonFirstSubjects + numCarolFirstSubjects;

subjectData = cell(numSubjects, NUM_COLUMNS);
subjectData(1:numCarolFirstSubjects, 1) = carolFirstCells{1,2}(CAROL_NUM_SKIP_LINES+1:end, 1);
subjectData(1:numCarolFirstSubjects, 2) = carolFirstCells{1,3}(CAROL_NUM_SKIP_LINES+1:end, 1);
subjectData(1:numCarolFirstSubjects, 3) = {'Carol'};
carolFirstCells = carolFirstCells(1,4:NUM_COLUMNS);
carolFirstCells = [carolFirstCells{:}];
subjectData(1:numCarolFirstSubjects, 4:end) = carolFirstCells(CAROL_NUM_SKIP_LINES+1:end, :);


subjectData(numCarolFirstSubjects+1:end, 1) = simonFirstCells{1,2}(SIMON_NUM_SKIP_LINES+1:end, 1);
subjectData(numCarolFirstSubjects+1:end, 2) = simonFirstCells{1,3}(SIMON_NUM_SKIP_LINES+1:end, 1);
subjectData(numCarolFirstSubjects+1:end, 3) = {'Simon'};
simonFirstCells = simonFirstCells(1,4:NUM_COLUMNS);
simonFirstCells = [simonFirstCells{:}];
subjectData(numCarolFirstSubjects+1:end, 4:end) = simonFirstCells(SIMON_NUM_SKIP_LINES+1:end, :);