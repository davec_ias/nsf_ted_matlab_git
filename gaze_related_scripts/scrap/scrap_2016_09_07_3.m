allFixationsSorted = sortrows(allFixations, 1);
allStarts = allFixations(:,1);
allEnds = allFixations(:,2);
changeTimes = sortrows(unique([allStarts; allEnds]));
uniqueStarts = unique(allFixations(:,1));
uniqueEnds = unique(allFixations(:,2));

%todo more efficent computation of this using vector ops
%alternately, impl via one pass iteration and no vector ops
fixationCounts = zeros(numel(changeTimes)+1, 3);
fixationCounts(2:end, 1) = changeTimes;
for i = 1:numel(changeTimes)+1
	fixationCounts(i, 2) = sum(allStarts <= fixationCounts(i,1)) - sum(allEnds <= fixationCounts(i,1));
	if (mod(i,5000) == 0), fprintf('%d of %d\n', i, numel(changeTimes)+1); end
end
%each count needs to be associated with a duration for weighting purposes
%while we average over time later
fixationCounts(:, 3) = [(fixationCounts(2:end,1) - fixationCounts(1:end-1,1)); 0];
%sum(allStarts < 7)

return

%% scrap
foo = [3, 4; 7,3; 2, 3];
foo = sortrows(foo, 1);
stairs(foo(:,1), foo(:,2));
xlim([0,10]);
ylim([0,10]);