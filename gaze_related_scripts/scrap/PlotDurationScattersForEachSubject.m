function PlotDurationScattersForSpecifiedSubjectBothVideos(aSubjectsToProcess)
NUM_FIG_ROWS = 3;
NUM_FIG_COLS = 2;
xLims = [0 720];
yLims = [0 5500];


curRow = NUM_FIG_ROWS + 1;
for i = 1:numel(aSubjectsToProcess)
	curID = aSubjectsToProcess{i};
	curData = GetValidSubjectData(curID);
	assert(~isempty(curData));	
	curGender = curData{2};
	curAge = curData{3};
	%curShownFirst = curData{4};
	
	carolFixationData = curData(5:9);
	simonFixationData = curData(10:14);
	
	if curRow > NUM_FIG_ROWS
		figure('position', [100, 100, 1600, 800]);
		curRow = 1;
	end
	subplot(NUM_FIG_ROWS, NUM_FIG_COLS, ((curRow-1)*NUM_FIG_COLS) + 1);
	PlotDurationsOneSubjectWholeVideo(carolFixationData, 'Carol', curID, curGender, curAge, xLims, yLims);
	
	subplot(NUM_FIG_ROWS, NUM_FIG_COLS, ((curRow-1)*NUM_FIG_COLS) + 2);
	PlotDurationsOneSubjectWholeVideo(simonFixationData, 'Simon', curID, curGender, curAge, xLims, yLims);
	curRow = curRow + 1;
end
