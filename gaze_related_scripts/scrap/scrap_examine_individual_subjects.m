FORCE_RELOAD = false;
if ((~(exist('glbValidSubjects', 'var') == 1)) || FORCE_RELOAD)
	load_data_from_disk;
end

%subjectsToProcess = {'sa1'; 'rn1'; 'an1'; 'sw1'}; %if you want all, subjects, use: subjectsToProcess = glbValidSubjects(2:end, 1);
subjectsToProcess = {'sa1'; 'rn1'; 'azz1'; 'lr1'}; %if you want all, subjects, use: subjectsToProcess = glbValidSubjects(2:end, 1);
subjectsToProcess = glbValidSubjects(4:8, 1);
close all;
%PlotDurationScattersSpecifiedSubjectBothVideos(subjectsToProcess);
startTimeSec = 139.0;
stopTimeSec = 145.0;
figure;
OverlayFixationsOnVideo(SIMON_IDX, startTimeSec, stopTimeSec, subjectsToProcess);
return
startTimeSec = 15.0;
stopTimeSec = 25.0;
figure;
OverlayFixationsOnVideo(CAROL_IDX, startTimeSec, stopTimeSec, subjectsToProcess);

startTimeSec = 60.0;
stopTimeSec = 70.0;
figure;
OverlayFixationsOnVideo(CAROL_IDX, startTimeSec, stopTimeSec, subjectsToProcess);

startTimeSec = 260.0;
stopTimeSec = 265.0;
figure;
OverlayFixationsOnVideo(CAROL_IDX, startTimeSec, stopTimeSec, subjectsToProcess);

startTimeSec = 462.0;
stopTimeSec = 466.0;
figure;
OverlayFixationsOnVideo(CAROL_IDX, startTimeSec, stopTimeSec, subjectsToProcess);

startTimeSec = 562.0;
stopTimeSec = 566.0;
figure;
OverlayFixationsOnVideo(CAROL_IDX, startTimeSec, stopTimeSec, subjectsToProcess);