subplot(4,4,3);
hold on;
pd = fitdist(windowedFixationCountsBySubject(3, :)', 'Poisson');
y = pdf(pd, binLabels);
plot(binLabels,y,'LineWidth',1, 'Color', 'Red');