close all;
graph_active_subjects_versus_scenes;

hold on;
h = zeros(9, 1);
textLabels = {};
for i = 0:8
	h(i+1) = plot(-100,-100,'s', 'visible', 'on', 'color', ShotTypeIDToColorRGB(i), 'MarkerFaceColor', ShotTypeIDToColorRGB(i));
	textLabels{i+1} = ShotTypeIDToDescriptionString(i); %#ok<SAGROW>
end
legend(h, textLabels);

return
figure;
scatter(allStarts / 1000, allDurations / 1000, .5, '.');

hold on;
for i = 1:numel(startTimesFrameNums)
	line([startTimesFrameNums(i)/FPS startTimesFrameNums(i)/FPS], [0 20], 'Color', 'black', 'LineStyle', '--');
end

return
SHOT_ANNOTATION_LOCATION = 'C:\davechisholm_google_drive\ted_project\first_four_videos_annotations\carol_dweck_the_power_of_believing_that_you_can_improve.20160330_021338.annotation';
[startTimesFrameNums, shotTypesIDNums, ~, ~] = ReadShotAnnotationFile(SHOT_ANNOTATION_LOCATION);
figure;
histogram(shotTypesIDNums, 0:7);

Number of subjects actively fixating over time Carol Dweck
(1000ms < duration < 2000ms threshold applied, no windowing)


Number of subjects actively fixating over time Simon Sinek
(1000ms < duration < 2000ms threshold applied, no windowing)