%output files for gesture annotation seem to be tab delimited and broken
%into two halfs. Each gesture has one line in each half. first half of file
%tells us what type of gesture, second half gives any extra annotated notes
%line format is always:
%TOOL_ID BLANK START_TIME_MS END_TIME_MS DURATION_MS TEXT_INFO
%where TOOL_ID is gesture_general4 in 1st half, gesture_description in 2nd
%BLANK is literally blank, nothing between the tabs
%next three are self explanatory
%TEXT_INFO is deictics, metaphoric, beats, or iconic in 1st half
%in 2nd half, TEXT info is whatever was typed in for that gesture as a free
%hand description. Doesn't ever seem to be blank
%note that cols 2-5 should be identical in the 1st and 2nd half

clear all;
testfile = 'C:\davechisholm_google_drive\ted_project\gesture_annotations\carol.gesture_annotation'
TEST_SPEC = '%q%q%u%u%u%q\n';
fID = fopen(testfile, 'r');
assert(fID ~= -1, 'Unable to open file for reading "%s"', testfile);
foo = textscan(fID, TEST_SPEC, 'Delimiter','\t');
fclose(fID);
gestureCell = ReadGestureAnnotationFile(testfile);

return

CAROL_NUM_SKIP_LINES = 1;
CAROL_FIRST_FILENAME = fullfile(TED_FILE_ROOT, 'survey_output\carol_first.csv');

SIMON_NUM_SKIP_LINES = 1;
SIMON_FIRST_FILENAME = fullfile(TED_FILE_ROOT, 'survey_output\simon_first.csv');

NUM_COLUMNS = 37;
CORRECT_ANSWERS = logical([...
	0 0 1 0 1 0 1 ... //CEG
	0 1 0 1 1 0 1 ... /BDEG
	0 0 0 1 0 0 1 ... //DG
	1 0 1 1 0 0 1 ... //ACDG
	0 0 1 0 1 0 0 ... // CE
	0 1 0 0 1 0 1 ... // BEG
]);

NUM_ANSWERS = 42;

fID = fopen(CAROL_FIRST_FILENAME, 'r');
assert(fID ~= -1, 'Unable to open file for reading "%s"', CAROL_FIRST_FILENAME);
carolFirstCells = textscan(fID, CSV_LINE_FORMAT_SPEC, 'Delimiter',',');
fclose(fID);

fID = fopen(SIMON_FIRST_FILENAME, 'r');
assert(fID ~= -1, 'Unable to open file for reading "%s"', SIMON_FIRST_FILENAME);
simonFirstCells = textscan(fID, CSV_LINE_FORMAT_SPEC, 'Delimiter',',');
fclose(fID);

numCarolFirstSubjects = size(carolFirstCells{2}, 1) - CAROL_NUM_SKIP_LINES;
numSimonFirstSubjects = size(simonFirstCells{2}, 1) - SIMON_NUM_SKIP_LINES;

numSubjects = numSimonFirstSubjects + numCarolFirstSubjects;

subjectData = cell(numSubjects, NUM_COLUMNS);
subjectData(1:numCarolFirstSubjects, 1) = carolFirstCells{1,2}(CAROL_NUM_SKIP_LINES+1:end, 1);
subjectData(1:numCarolFirstSubjects, 2) = carolFirstCells{1,3}(CAROL_NUM_SKIP_LINES+1:end, 1);
subjectData(1:numCarolFirstSubjects, 3) = {'Carol'};
carolFirstCells = carolFirstCells(1,4:NUM_COLUMNS);
carolFirstCells = [carolFirstCells{:}];
subjectData(1:numCarolFirstSubjects, 4:end) = carolFirstCells(CAROL_NUM_SKIP_LINES+1:end, :);


subjectData(numCarolFirstSubjects+1:end, 1) = simonFirstCells{1,2}(SIMON_NUM_SKIP_LINES+1:end, 1);
subjectData(numCarolFirstSubjects+1:end, 2) = simonFirstCells{1,3}(SIMON_NUM_SKIP_LINES+1:end, 1);
subjectData(numCarolFirstSubjects+1:end, 3) = {'Simon'};
simonFirstCells = simonFirstCells(1,4:NUM_COLUMNS);
simonFirstCells = [simonFirstCells{:}];
subjectData(numCarolFirstSubjects+1:end, 4:end) = simonFirstCells(SIMON_NUM_SKIP_LINES+1:end, :);

