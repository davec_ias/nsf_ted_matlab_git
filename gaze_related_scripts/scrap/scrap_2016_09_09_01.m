close all;
maxDuration = max(allEnds);

%Y_LIM = [0 .5];
WINDOW_WIDTH_MS = 1000;
WINDOW_SHIFT_MS = 1000;

overhang = WINDOW_WIDTH_MS - WINDOW_SHIFT_MS;
numWindows = floor((maxDuration - overhang) / WINDOW_SHIFT_MS);

windowedFixationCountsBySubject = zeros(numSubj, numWindows);

for i_subj = 1:numSubj
	curSubjData = subjectData{i_subj};
	
	for j_window = 1:numWindows
		startTimeMS = (j_window * WINDOW_SHIFT_MS) - WINDOW_SHIFT_MS;
		endTimeMS = startTimeMS + WINDOW_WIDTH_MS;
		
		debugInfo = (curSubjData(:,1) >= startTimeMS) & (curSubjData(:,1) < endTimeMS);
		curCount = sum((curSubjData(:,1) >= startTimeMS) & (curSubjData(:,1) < endTimeMS));
		windowedFixationCountsBySubject(i_subj, j_window) = curCount;
	end
end


windowedFixationCountsAllSubjects = windowedFixationCountsBySubject(:)';

numBins = max(windowedFixationCountsAllSubjects) + 1;
fixationCountHistBySubject = zeros(numSubj, numBins);

for i_subj = 1:numSubj
	curSubjData = windowedFixationCountsBySubject(i_subj, :);
	
	for j_bin = 1:numBins
		fixationCountHistBySubject(i_subj, j_bin) = sum(curSubjData == j_bin-1); %-1 due to matlab one-based array indexing
	end
	
	%normalize to 1
	fixationCountHistBySubject(i_subj, :) = fixationCountHistBySubject(i_subj, :) ./ numWindows;
end

fixationCountHistAllSubjects = sum(fixationCountHistBySubject) / numSubj; %div for normalization

binLabels = 0:numBins-1;

NUM_FIGURE_ROW = 5;
NUM_FIGURE_COL = 3;
NUM_PLOT_PER_FIG = NUM_FIGURE_ROW * NUM_FIGURE_COL;

%yLim = [0 max([fixationCountHistBySubject(:); fixationCountHistAllSubjects(:)]) * 1.2];


for i_subj = 1:numSubj
	curSubjData = fixationCountHistBySubject(i_subj, :);
	
	curSubPlotNum = mod(i_subj, NUM_PLOT_PER_FIG);
	if (curSubPlotNum == 0), curSubPlotNum = NUM_PLOT_PER_FIG; end
	if (curSubPlotNum == 1), figure; end
	
	subplot(NUM_FIGURE_ROW, NUM_FIGURE_COL, curSubPlotNum);
	bar(binLabels, curSubjData);
	xlim([0 numBins]); ylim(Y_LIM);
	if (i_subj == 3)
		ylim(Y_LIM .* 2);; %quick fix for bad data
	end
	title(sprintf('Subj. %d, %ds win', i_subj, WINDOW_WIDTH_MS / 1000));
	
	hold on;
	pd = fitdist(windowedFixationCountsBySubject(i_subj, :)', 'Poisson');
	y = pdf(pd, binLabels);
	plot(binLabels,y,'LineWidth',1, 'Color', 'Red');
	
	
	pd = fitdist(windowedFixationCountsBySubject(i_subj, :)', 'Normal');
	y = pdf(pd, binLabels);
	plot(binLabels,y,'LineWidth',1, 'Color', 'Blue');
	%legend('Actual histogram', 'Best Fit Poisson', 'Best Fit Normal');
	hold off;
end

figure;
bar(binLabels, fixationCountHistAllSubjects);
xlim([0 numBins]); ylim(Y_LIM);
title(sprintf('Histogram of # of fixations per window\n(Avg. over all subjects, windows with %ds sec width and %ds shift)', ...
		WINDOW_WIDTH_MS / 1000, WINDOW_SHIFT_MS / 1000));
	
hold on;
pd = fitdist(windowedFixationCountsAllSubjects', 'Poisson');
y = pdf(pd, binLabels);
plot(binLabels,y,'LineWidth',1, 'Color', 'Red');


pd = fitdist(windowedFixationCountsAllSubjects', 'Normal');
y = pdf(pd, binLabels);
plot(binLabels,y,'LineWidth',1, 'Color', 'Blue');
legend('Actual histogram', 'Best Fit Poisson', 'Best Fit Normal');

ylabel('Normalized Probability'); xlabel('Number of Fixations');

hold off;