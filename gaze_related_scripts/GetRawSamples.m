function [rawGazeSampleCell] = GetRawSamples()

global DATA_ROOT_DIRECTORY;

rawGazeSampleCell = {
	'VideoName', 'NumSafeSamples', 'SubjectNames', 'RawSamplesStruct', 'ZScorePupilDataMatrix', 'PCACoeff', 'PCAExplained', 'PCAMean';
	'simon', 718900, {}, {}, [], [], [], [];
	'carol', 624600, {}, {}, [], [], [], [];
};

for vid_i = 2:size(rawGazeSampleCell, 1)
	%info hardcoded in our struct above
	curVidName = rawGazeSampleCell{vid_i, 1};
	curVidDataFileLoc = fullfile(DATA_ROOT_DIRECTORY, 'raw_gaze_samples_mat_files', [curVidName '_rawGaze.mat']);
	curVidNumSamples = rawGazeSampleCell{vid_i, 2};

	%get relevant data from mat file provided by Linbi
	S = load(curVidDataFileLoc);
	fieldNames = fieldnames(S); assert(numel(fieldNames) == 1);
	eval(['curStruct = S.' fieldNames{1} ';']);
	rawGazeSampleCell{vid_i, 3} = curStruct.data(:,1);
	rawGazeSampleCell{vid_i, 4} = curStruct.data(:,2);
	
	
	%extract the actual gaze samples into a matrix format suitable for PCA
	curNumSubjects = size(rawGazeSampleCell{vid_i, 3}, 1);
	rawGazeSampleCell{vid_i, 5} = zeros(curNumSubjects, curVidNumSamples);
	for subj_j = 1:curNumSubjects
		rawGazeSampleCell{vid_i, 5}(subj_j, :) = rawGazeSampleCell{vid_i, 4}{subj_j}(1:curVidNumSamples)';
	end
	
	rawGazeSampleCell{vid_i, 5} = zscore(rawGazeSampleCell{vid_i, 5}, 0, 2);
	%[rawGazeSampleCell{vid_i, 6}, rawGazeSampleCell{vid_i, 7}, ~] = pca(rawGazeSampleCell{vid_i, 5});
	[coeff,~,~,~,explained,mu] = pca(rawGazeSampleCell{vid_i, 5});
	rawGazeSampleCell{vid_i, 6} = coeff';
	rawGazeSampleCell{vid_i, 7} = explained;
	rawGazeSampleCell{vid_i, 8} = mu;
end