clearvars;
load_consts;
%to assure no exceptions
for i = 0:8
	[result] = ShotTypeIDToDescriptionString(i);
	assert(numel(result) >= 5);
	[result] = ShotTypeIDToColorRGB(i);
	assert(numel(result) == 3);
end

clearvars;