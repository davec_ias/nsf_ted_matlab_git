%2016-09-21 davec@cs.columbia.edu
function [descriptionString] = ShotTypeIDToDescriptionString(aShotTypeID)
global SHOT_TYPES SHOT_TYPES_ID_OFFSET IDX_SHOT_TYPE_ID_NUM IDX_SHOT_TYPE_DESCRIPTION;
adjustedIndex = aShotTypeID + SHOT_TYPES_ID_OFFSET;
assert(size(SHOT_TYPES, 1) >= adjustedIndex);
descriptionString = SHOT_TYPES{adjustedIndex, IDX_SHOT_TYPE_DESCRIPTION};
return


%% original behavior below; left for reference but currently unused
switch aShotTypeID %#ok<UNRCH>
	case 0
		descriptionString = 'Intro/Outro';
	case 1
		descriptionString = 'Head-On Speaker';
	case 2
		descriptionString = 'Side/45 Speaker';
	case 3
		descriptionString = 'Audience';
	case 4
		descriptionString = 'Slides/Graphics';
	case 5
		descriptionString = 'Speaker From Distance';
	case 6
		descriptionString = 'Facial Close-up';
	case 7
		descriptionString = 'Onstage Demo';
	case 8
		descriptionString = 'Other';
	otherwise
		error('Unknown shot type ID %d', aShotTypeID);
end
