%2016-09-07 davec@cs.columbia.edu
global FIXATION_FILE_LOCATION VIDEO_TO_USE_OLD;
dirResults = dir(fullfile(FIXATION_FILE_LOCATION, ['*' VIDEO_TO_USE_OLD '*.xls']));
numSubj = numel(dirResults);
subjectData = cell(numSubj, 1);

for i = 1:numSubj
	curFile = fullfile(FIXATION_FILE_LOCATION, dirResults(i).name);
	fprintf('Loading "%s" from disk...\n', curFile);
	subjectData{i} = GetDataFromFixationFile(curFile);
end
currentlyLoadedVideoName = VIDEO_TO_USE_OLD;

clear dirResults i curfile