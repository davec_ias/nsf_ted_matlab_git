maxTime = max(allEnds);
numX = size(windowedFixationCountsBySubject, 2);
xValsMS = [0:WINDOW_SHIFT_MS_WINDOWED_COUNTS: (numX-1)* WINDOW_SHIFT_MS_WINDOWED_COUNTS] + (WINDOW_WIDTH_MS_WINDOWED_COUNTS / 2);
xValsSec = xValsMS / 1000;

for i_subj = 1:numSubj
	curSubjData = windowedFixationCountsBySubject(i_subj, :);
	
	curSubPlotNum = mod(i_subj, NUM_PLOT_PER_FIG);
	if (curSubPlotNum == 0), curSubPlotNum = NUM_PLOT_PER_FIG; end
	if (curSubPlotNum == 1), figure; set(gcf,'color','w'); end
	
	subplot(NUM_FIGURE_ROW, NUM_FIGURE_COL, curSubPlotNum);
	plot(xValsSec, curSubjData);
	ylim([0 numBins-1]); xlim([0 max(xValsSec) + 1]);
	title(sprintf('Subj. %d - # of fixations per window over time', i_subj));
	xlabel('Time (seconds)'); ylabel('Number of fixations');
end


figure; set(gcf,'color','w');
plot(xValsSec, sum(windowedFixationCountsBySubject) / numSubj);
ylim([0 numBins-1]); xlim([0 max(xValsSec) + 1]);
title(sprintf('# of fixations per window over time\n(Avg. over all subjects, windows with %ds sec width and %ds shift)', ...
		WINDOW_WIDTH_MS_WINDOWED_COUNTS / 1000, WINDOW_SHIFT_MS_WINDOWED_COUNTS / 1000));
xlabel('Time (seconds)'); ylabel('Number of fixations');
