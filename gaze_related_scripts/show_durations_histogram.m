close all;
NUM_FIGURE_ROW = 5;
NUM_FIGURE_COL = 3;
NUM_PLOT_PER_FIG = NUM_FIGURE_ROW * NUM_FIGURE_COL;
MAX_FIXATION_DURATION = 15000;

binWidth = 10;
binEdges = [0:binWidth:MAX_FIXATION_DURATION];
binLabels = binEdges(2:end);

for i_subj = 1:numSubj
	curData = subjectData{i_subj};
	
	curSubPlotNum = mod(i_subj, NUM_PLOT_PER_FIG);
	if (curSubPlotNum == 0), curSubPlotNum = NUM_PLOT_PER_FIG; end
	if (curSubPlotNum == 1), figure; set(gcf,'color','w'); end
	subplot(NUM_FIGURE_ROW, NUM_FIGURE_COL, curSubPlotNum);

	binCounts = histcounts(curData(:, IDX_FIX_DUR), binEdges);
	normalizedBinCounts = binCounts / (sum(binCounts) * binWidth);
	PlotHistogramAndFits(normalizedBinCounts, curData(:, IDX_FIX_DUR), -1, false, false, true, binLabels);

	xlim([0 4000]); title(sprintf('Subject %d', i_subj));
	set(gca, 'XTick', [0:500:max(allDurations)+binWidth]);
end


binCounts = histcounts(allDurations, binEdges);
normalizedBinCounts = binCounts / (sum(binCounts) * binWidth);


figure;  set(gcf,'color','w');
PlotHistogramAndFits(normalizedBinCounts, allDurations, -1, false, false, true, binLabels);

xlim([0 4000]); 
set(gca, 'XTick', [0:500:max(allDurations)+binWidth]);

title(sprintf('Histogram of Fixation Durations with Log-Normal distribution fit\n(All subjects, not thresholded)'));
xlabel('Fixation duration (milliseconds)');
ylabel(sprintf('Probability', binWidth));

return