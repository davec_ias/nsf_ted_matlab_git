

overhang = WINDOW_WIDTH_MS_WINDOWED_COUNTS - WINDOW_SHIFT_MS_WINDOWED_COUNTS;
numWindows = floor((max(allEnds) - overhang) / WINDOW_SHIFT_MS_WINDOWED_COUNTS);

windowedFixationCountsBySubject = zeros(numSubj, numWindows);

for i_subj = 1:numSubj
	curSubjData = subjectData{i_subj};
	
	for j_window = 1:numWindows
		startTimeMS = (j_window * WINDOW_SHIFT_MS_WINDOWED_COUNTS) - WINDOW_SHIFT_MS_WINDOWED_COUNTS;
		endTimeMS = startTimeMS + WINDOW_WIDTH_MS_WINDOWED_COUNTS;
		
		debugInfo = (curSubjData(:,1) >= startTimeMS) & (curSubjData(:,1) < endTimeMS);
		curCount = sum((curSubjData(:,1) >= startTimeMS) & (curSubjData(:,1) < endTimeMS));
		windowedFixationCountsBySubject(i_subj, j_window) = curCount;
	end
end


numBins = max(windowedFixationCountsBySubject(:)) + 1;
fixationCountHistBySubject = zeros(numSubj, numBins);

for i_subj = 1:numSubj
	curSubjData = windowedFixationCountsBySubject(i_subj, :);
	
	for j_bin = 1:numBins
		fixationCountHistBySubject(i_subj, j_bin) = sum(curSubjData == j_bin-1); %-1 due to matlab one-based array indexing
	end
	
	%normalize to 1
	fixationCountHistBySubject(i_subj, :) = fixationCountHistBySubject(i_subj, :) ./ numWindows;
end
