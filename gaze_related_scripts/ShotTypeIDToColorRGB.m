%2016-09-21 davec@cs.columbia.edu
function [colorRGB] = ShotTypeIDToColorRGB(aShotTypeID)
global	SHOT_TYPES SHOT_TYPES_ID_OFFSET IDX_SHOT_TYPE_ID_NUM IDX_SHOT_TYPE_RGB_TRIPLET;
adjustedIndex = aShotTypeID + SHOT_TYPES_ID_OFFSET;
assert(size(SHOT_TYPES, 1) >= adjustedIndex);
colorRGB = SHOT_TYPES{adjustedIndex, IDX_SHOT_TYPE_RGB_TRIPLET} / 255.0;
return

%% original behavior below; left for reference but currently unused

%types 1, 2, 4 most common. should be in range 0 - 8
%todo, how did I get these rgb magic numbers? Recall finding list of
%visually distinct colors on web
switch aShotTypeID
	case 0
		colorRGB = [15, 207, 192];
	case 1
		colorRGB = [247 156 212];
	case 2
		colorRGB = [142 6 59];
	case 3
		colorRGB = [133 149 225];
	case 4
		colorRGB = [239 151 8];
	case 5
		colorRGB = [234 211 198];
	case 6
		colorRGB = [214 188 192]; %[255 165 222]; %[214 188 192];
	case 7
		colorRGB = [17 198 56]';
	case 8
		colorRGB = [2 63 165];
	otherwise
		error('Unknown shot type ID %d', aShotTypeID);
end

colorRGB = colorRGB / 255.0;