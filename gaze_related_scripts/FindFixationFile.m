%returns path to a fixation file, or empty string if it doesn't exist
function [fullLocationOfFixationFile] = FindFixationFile(aFixationDir, aSubjectID, aVideoID);

fullLocationOfFixationFile = '';
searchString = fullfile(aFixationDir, [aSubjectID '_' aVideoID '*.xls']);
dirResults = dir(searchString);
if numel(dirResults) == 0
	return
elseif numel(dirResults) > 1
	warning('Found more than one possible matching file looking for fixation file using dir search string "%s"', searchString);
	return
end
assert(~ dirResults(1).isdir);
fullLocationOfFixationFile = fullfile(aFixationDir, dirResults(1).name);

