%2016-09-20 davec@cs.columbia.edu
%a quick and dirty script to compare the results of using the automatic
%shot detection utility detailed at http://mklab.iti.gr/project/video-shot-segm
%to the hand annotation

load_consts;

load_automatic_shot_detection_results;

SHOT_ANNOTATION_LOCATION = 'C:\davechisholm_google_drive\ted_project\first_four_videos_annotations\carol_dweck_the_power_of_believing_that_you_can_improve.20160330_021338.annotation';
[startTimesFrameNums, ~, ~, ~] = ReadShotAnnotationFile(SHOT_ANNOTATION_LOCATION);
annotatedShotFrameNumsFirst9min = startTimesFrameNums(startTimesFrameNums < automaticEndTime);



close all;
figure; set(gcf,'color','w');
xlim([0 automaticEndTime/FPS]); ylim([0 1]);

for i = 1:numel(automaticShotFrameNums)
	line([automaticShotFrameNums(i)/FPS automaticShotFrameNums(i)/FPS], [0 .5], 'Color', 'red');
end

for i = 1:numel(annotatedShotFrameNumsFirst9min)
	line([annotatedShotFrameNumsFirst9min(i)/FPS annotatedShotFrameNumsFirst9min(i)/FPS], [.5 1.0], 'Color', 'blue');
end

set(gca,'ytick',[]);
xlabel('Time (seconds)')
title(sprintf('Annotated shots (blue) vs automatically detected shots (red)\n (Carol Dweck 0:00-5:40)'));


return

%% scrap
fileID = fopen(SHOT_ANNOTATION_LOCATION);
shotAnnotations = textscan(fileID,'%d%d%s%s', 'HeaderLines', 1, 'Delimiter', ',');
annotatedShotFrameNumsFirst9min = shotAnnotations{1};
annotatedShotFrameNumsFirst9min = annotatedShotFrameNumsFirst9min(annotatedShotFrameNumsFirst9min < automaticEndTime);
fclose(fileID);
