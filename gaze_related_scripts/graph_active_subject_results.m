%2016-09-07 davec@cs.columbia.edu

figure; set(gcf,'color','w');
yLabelText = sprintf('Num. Subjects Currently Fixated (of %d subjects)', numSubj);
xLabelText = 'Time (seconds)';
yMin = 0; yMax = numSubj;


subplot(2,1,1);
plot(activeSubjectCounts(:,1) / 1000, activeSubjectCounts(:,2));
title('Raw counts of number of subjects actively fixating over time');
ylim([yMin yMax]); ylabel(yLabelText); xlabel(xLabelText);


subplot(2,1,2);
stairs(windowedAvgs(:,1) / 1000, windowedAvgs(:,2));
title(sprintf('Windowed avg. of above\n(%d sec width, %dsec shift)', ...
		WINDOW_WIDTH_MS_ACTIVE_FIXATIONS / 1000, WINDOW_SHIFT_MS_ACTIVE_FIXATIONS / 1000));
ylim([yMin yMax]); ylabel(yLabelText); xlabel(xLabelText);


figure; set(gcf,'color','w');
bar(histLabels, histCounts);
title('Histogram of number of active fixations (over each millisecond)');
xlabel(sprintf('Num. Subjects Simultaneously Fixated (of %d subjects)', numSubj));
ylabel('Probability');

hold on;
pd = fitdist(activeSubjectCountsByMS, 'Poisson');
y = pdf(pd, histLabels);
plot(histLabels,y,'LineWidth',1, 'Color', 'Red');


pd = fitdist(activeSubjectCountsByMS, 'Normal');
y = pdf(pd, histLabels);
plot(histLabels,y,'LineWidth',1, 'Color', 'Blue');
hold off;