function [ValidSubjectData] = GetValidSubjectData(aSubjectID)
global glbValidSubjects;
for i = 2:size(glbValidSubjects, 1)
	if strcmpi(aSubjectID, glbValidSubjects{i, 1})
		ValidSubjectData = glbValidSubjects(i, :);
		return
	end
end
ValidSubjectData = [];