%2016-09-07 davec@cs.columbia.edu
endOfVideoMS = max(activeSubjectCounts(:, 1));

windowStartMS = 0;
windowedAvgs = [];
while (true)
	windowEndMS = windowStartMS + WINDOW_WIDTH_MS_ACTIVE_FIXATIONS;
	windowCenterMS = (windowStartMS + windowEndMS) / 2;
	if windowEndMS > endOfVideoMS, break; end
	
	indicesOfRelevantPoints = (activeSubjectCounts(:,1) >= windowStartMS) & (activeSubjectCounts(:,1) < windowEndMS);
	relevantPoints = activeSubjectCounts(indicesOfRelevantPoints, :);

	%a simple avg would be misleading, and count a point that lasts for
	%10ms the same as one that lasts for 50ms. so weighed avg by duration
	weightedValues = relevantPoints(:,2) .* relevantPoints(:,3);
	totalWeight = sum(relevantPoints(:,3));
	
	avgValue = sum(weightedValues) / totalWeight;
	if isnan(avgValue), avgValue = 0; end
	
	windowedAvgs = [windowedAvgs; windowCenterMS avgValue]; %#ok<AGROW>
	windowStartMS = windowStartMS + WINDOW_SHIFT_MS_ACTIVE_FIXATIONS;
end

