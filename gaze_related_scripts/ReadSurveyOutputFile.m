%returns a cell array containing the raw survey output
%typically, further processing (e.g. strip header, translate selection
%answers to binary, etc) is needed
function [subjectResponsesCell, headerCell] = ReadSurveyOutputFile(aFilename)
SURVEY_LINE_FORMAT_SPEC = '%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q\n';
%global	NUM_SURVEY_COLUMNS_TO_PROCESS;
NUM_COLUMNS_TO_PROCESS = 37; 


fileID = fopen(aFilename, 'r');
cleaner = onCleanup(@() fclose(fileID));
if (fileID < 0)
	error('Unable to open file for reading "%s"', aFilename);
end
subjectResponsesCell = textscan(fileID, SURVEY_LINE_FORMAT_SPEC, 'Delimiter',',');
subjectResponsesCell = [subjectResponsesCell{1:NUM_COLUMNS_TO_PROCESS}];
headerCell = subjectResponsesCell(1, :);
subjectResponsesCell = subjectResponsesCell(2:end, :);
