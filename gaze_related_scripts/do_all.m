%2016-09-07 davec@cs.columbia.edu
%to run, change the ROOT location in load_consts to a directory containing
%fixation reports. It will process any and all files in that directory 
%with "*.xls" extension 

clearvars -except subjectData numSubj currentlyLoadedVideoName; %#ok<UNRCH> 
load_consts;

%reload if flag set, or video name changed
if (FORCE_DATA_RELOAD) || (exist('currentlyLoadedVideoName', 'var') && ~strcmp(currentlyLoadedVideoName, VIDEO_TO_USE_OLD))
	clear subjectData numSubj subjectDataLoadedFor;
	%close all; 
end


if (~exist('subjectData', 'var'))
	load_fixations;
end

global SHOT_ANNOTATION_LOCATION_OLD;
[shotStartFrameNums, shotTypesIDNums, shotStartTimesTextual, shotTypesTextual] = ...
		ReadShotAnnotationFile(SHOT_ANNOTATION_LOCATION_OLD);
prepare_and_filter_fixations;


if (DO_HISTOGRAM_OF_FIXATION_DURATION)
	show_durations_histogram;
end

if (DO_ACTIVE_FIXATIONS)
	% impl and display of "number of active fixations at any moment"
	compute_active_subject_counts;
	compute_windowed_avg_active_subject_counts;
	compute_active_subject_counts_histogram;
	graph_active_subject_results;
	graph_active_subjects_versus_scenes;
end

if (DO_WINDOWED_FIXATION_COUNTS)
	% impl and display of "number of fixations in a window"
	compute_windowed_fixation_counts;
	graph_windowed_fixation_counts;
	graph_windowed_fixation_counts_over_time;
end

if (DO_DURATION_SCATTER_PLOTS)
	show_duration_scatter_plots;
end
