%2016-09-21 davec@cs.columbia.edu
function [ ...
		shotStartFrameNums, ...
		shotTypesIDNums, ...
		shotStartTimesTextual, ...
		shotTypesTextual] = ...
	ReadShotAnnotationFile(aFilename)


fileID = fopen(aFilename);
cleaner = onCleanup(@() fclose(fileID));
shotAnnotations = textscan(fileID,'%d%d%s%s', 'HeaderLines', 1, 'Delimiter', ',');
shotStartFrameNums = shotAnnotations{1};
shotTypesIDNums = shotAnnotations{2};
shotStartTimesTextual = shotAnnotations(3);
shotTypesTextual =  shotAnnotations(4);
