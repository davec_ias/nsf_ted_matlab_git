assert(exist('glbVideoInfos', 'var') == 1);

for i = 2:size(glbVideoInfos, 1)
	curID = glbVideoInfos{i, IDX_VIDEO_STR_ID};
	curVideoFileName = glbVideoInfos{i, IDX_VIDEO_FILENAME};
	curReader = VideoReader(curVideoFileName); %#ok<TNMLP>
	
	if ((curReader.Height ~= ORIGINAL_VIDEO_HEIGHT_PX) || (curReader.Width ~= ORIGINAL_VIDEO_WIDTH_PX))
		warning('Video %s has unexpected dim (%d x %d), expected (%d x %d)', ...
				curFilecurVideoFileNameName, curReader.Height, curReader.Width, ORIGINAL_VIDEO_HEIGHT_PX, ORIGINAL_VIDEO_WIDTH_PX);
	end
	
	curShotAnnoCell = {NaN, NaN};
	curShotAnnoFileName = fullfile(VIDEO_SHOT_ANNOTATIONS_LOCATION, [curID '.shot_annotation']);
	[curShotAnnoCell{1}, curShotAnnoCell{2}, ~, ~] = ReadShotAnnotationFile(curShotAnnoFileName);
	
	
	curGestureAnnoFileName = fullfile(VIDEO_GESTURE_ANNOTATIONS_LOCATION, [curID '.gesture_annotation']);
	curGestureAnnoCell = ReadGestureAnnotationFile(curGestureAnnoFileName);
	
	glbVideoInfos{i, IDX_VIDEO_READER} = curReader; %#ok<SAGROW>
	glbVideoInfos{i, IDX_VIDEO_SHOT_ANNO} = curShotAnnoCell; %#ok<SAGROW>
	glbVideoInfos{i, IDX_VIDEO_GESTURE_ANNO} = curGestureAnnoCell; %#ok<SAGROW>
	
end

clear i curID curVideoFileName curReader curShotAnnoFileName curShotAnnoCell curGestureAnnoFileName curGestureAnnoCell;