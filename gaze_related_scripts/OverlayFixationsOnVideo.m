function OverlayFixationsOnVideo(aVideoIndex, aStartTimeSec, aStopTimeSec, aSubjectsToProcess)
assert((0 <= aStartTimeSec) && (aStopTimeSec >= aStartTimeSec));

drawArrow = @(x,y,varargin) quiver( x(1),y(1),x(2)-x(1),y(2)-y(1),0, varargin{:} );%see http://stackoverflow.com/questions/25729784/how-to-draw-an-arrow-in-matlab

TYPICAL_DURATION_MS = 300;
MIN_FACTOR = .2; MAX_FACTOR = 5; DIAMETER_PIXELS = 20;
TEXT_HEIGHT = 20;
LEFT_PAD = 20;

global glbVideoInfos IDX_VIDEO_STR_ID IDX_VIDEO_READER;
global	GAZE_SCREEN_WIDTH_PX;
global	GAZE_SCREEN_HEIGHT_PX;
global	ORIGINAL_VIDEO_WIDTH_PX;
global	ORIGINAL_VIDEO_HEIGHT_PX;
global	CAROL_IDX SIMON_IDX;


videoName = glbVideoInfos{aVideoIndex, IDX_VIDEO_STR_ID};
		
canvas = uint8(ones(GAZE_SCREEN_HEIGHT_PX,GAZE_SCREEN_WIDTH_PX,3));
widthMargin = floor(GAZE_SCREEN_WIDTH_PX - ORIGINAL_VIDEO_WIDTH_PX) / 2;
heightMargin = floor(GAZE_SCREEN_HEIGHT_PX - ORIGINAL_VIDEO_HEIGHT_PX) / 2;


%videoName = glbVideoInfos{aVideoIndex, IDX_VIDEO_STR_ID};
reader = glbVideoInfos{aVideoIndex, IDX_VIDEO_READER};

reader.CurrentTime = mean([aStartTimeSec aStopTimeSec]);
frame = readFrame(reader);
canvas(		heightMargin:heightMargin+ORIGINAL_VIDEO_HEIGHT_PX-1, ...
				widthMargin:widthMargin+ORIGINAL_VIDEO_WIDTH_PX-1, ...
		:) = frame;

timeIncrSec = (aStopTimeSec - aStartTimeSec) / 3;
for j_fix = 0:3
	reader.CurrentTime = aStartTimeSec + j_fix*timeIncrSec;
	frame = readFrame(reader);
	frame = imresize(frame, .25);
	newWidth = size(frame, 2);
	newHeight = size(frame, 1);
	startX = j_fix*(GAZE_SCREEN_WIDTH_PX/4)+21;
	startY = 1;
	canvas(	startY:startY +newHeight-1, ...
				startX:startX +newWidth-1, ...
		:) = frame;
end
	
imshow(canvas);

global NINE_DISTINCT_COLORS;

for i_subj = 1:numel(aSubjectsToProcess)
	curSubjID = aSubjectsToProcess{i_subj};
	curData = GetValidSubjectData(curSubjID);
	%todo fix this hack
	if (aVideoIndex == CAROL_IDX)
		fixColumn = 5;
	elseif (aVideoIndex == SIMON_IDX)
		fixColumn = 10;
	else
		error('Unknown aVideoIndex');
	end
	fixations = curData{fixColumn};
	startTimesMS = fixations(:, 1); durationsMS = fixations(:, 2); 
	Xs = fixations(:, 4); Ys = fixations(:, 5); 
	matchingIndices = (startTimesMS >= aStartTimeSec * 1000) & (startTimesMS < aStopTimeSec * 1000);
	matchingDurationsMS = durationsMS(matchingIndices);
	matchingXs = Xs(matchingIndices);
	matchingYs = Ys(matchingIndices);

	colorToUse = NINE_DISTINCT_COLORS{mod(i_subj-1, numel(NINE_DISTINCT_COLORS)) +1} / 255.0;%[1 1 0];
	prev = [];
	t = linspace(0, 2*pi);
	for j_fix = 1:numel(matchingDurationsMS)
		curDuration = matchingDurationsMS(j_fix);
		sizeFactor = max(MIN_FACTOR, min(MAX_FACTOR, sqrt((curDuration / TYPICAL_DURATION_MS))));
		diameter = DIAMETER_PIXELS * sizeFactor; radius = diameter / 2;

		x = matchingXs(j_fix); y =  matchingYs(j_fix); 
		x = min(GAZE_SCREEN_WIDTH_PX-radius, max(radius, x));
		y = min(GAZE_SCREEN_HEIGHT_PX-radius, max(radius, y));

		circleX = radius*cos(t) + x;
		circleY = radius*sin(t) + y;

		if j_fix == 1
			eAlpha = 1;
			lw = 2;
		else
			eAlpha = 1; 
			lw=.5;
		end
		patch(circleX, circleY, colorToUse, 'FaceAlpha', .5, 'EdgeAlpha', eAlpha, 'linewidth',lw);

		if isempty(prev)
			%text(x+radius, y+radius, curSubjID, 'Color', [0 0 1]);
		else
			hold on;
			drawArrow([prev(1) x], [prev(2) y], 'color', colorToUse, 'linewidth',1);	
		end
		prev = [x y];
	end
	x = LEFT_PAD; y = heightMargin + (i_subj*TEXT_HEIGHT);
	circleX = DIAMETER_PIXELS*cos(t)/2 + x;
	circleY = DIAMETER_PIXELS*sin(t)/2 + y;
	patch(circleX, circleY, colorToUse, 'FaceAlpha', .5, 'EdgeAlpha', 1, 'linewidth',2);
	text(x+DIAMETER_PIXELS, y, curSubjID, 'Color', colorToUse, 'FontWeight', 'Bold');
end

title(sprintf('%s - %4.1f to %4.1f seconds', videoName, aStartTimeSec, aStopTimeSec));
