function [surveyOutput] = ReadAllSurveyOutput(aFilename)
CSV_LINE_FORMAT_SPEC = '%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q\n';
global DATA_ROOT_DIRECTORY;
NUM_COLUMNS = 37;
CAROL_NUM_SKIP_LINES = 1;
CAROL_FIRST_FILENAME = fullfile(DATA_ROOT_DIRECTORY, 'survey_output\carol_first.csv');
SIMON_NUM_SKIP_LINES = 1;
SIMON_FIRST_FILENAME = fullfile(DATA_ROOT_DIRECTORY, 'survey_output\simon_first.csv');


fID = fopen(CAROL_FIRST_FILENAME, 'r');
assert(fID ~= -1, 'Unable to open file for reading "%s"', CAROL_FIRST_FILENAME);
carolFirstCells = textscan(fID, CSV_LINE_FORMAT_SPEC, 'Delimiter',',');
fclose(fID);

fID = fopen(SIMON_FIRST_FILENAME, 'r');
assert(fID ~= -1, 'Unable to open file for reading "%s"', SIMON_FIRST_FILENAME);
simonFirstCells = textscan(fID, CSV_LINE_FORMAT_SPEC, 'Delimiter',',');
fclose(fID);

numCarolFirstSubjects = size(carolFirstCells{2}, 1) - CAROL_NUM_SKIP_LINES;
numSimonFirstSubjects = size(simonFirstCells{2}, 1) - SIMON_NUM_SKIP_LINES;

numSubjects = numSimonFirstSubjects + numCarolFirstSubjects;

surveyOutput = cell(numSubjects, NUM_COLUMNS);
surveyOutput(1:numCarolFirstSubjects, 1) = carolFirstCells{1,2}(CAROL_NUM_SKIP_LINES+1:end, 1);
surveyOutput(1:numCarolFirstSubjects, 2) = carolFirstCells{1,3}(CAROL_NUM_SKIP_LINES+1:end, 1);
surveyOutput(1:numCarolFirstSubjects, 3) = {'Carol'};
carolFirstCells = carolFirstCells(1,4:NUM_COLUMNS);
carolFirstCells = [carolFirstCells{:}];
surveyOutput(1:numCarolFirstSubjects, 4:end) = carolFirstCells(CAROL_NUM_SKIP_LINES+1:end, :);


surveyOutput(numCarolFirstSubjects+1:end, 1) = simonFirstCells{1,2}(SIMON_NUM_SKIP_LINES+1:end, 1);
surveyOutput(numCarolFirstSubjects+1:end, 2) = simonFirstCells{1,3}(SIMON_NUM_SKIP_LINES+1:end, 1);
surveyOutput(numCarolFirstSubjects+1:end, 3) = {'Simon'};
simonFirstCells = simonFirstCells(1,4:NUM_COLUMNS);
simonFirstCells = [simonFirstCells{:}];
surveyOutput(numCarolFirstSubjects+1:end, 4:end) = simonFirstCells(SIMON_NUM_SKIP_LINES+1:end, :);

