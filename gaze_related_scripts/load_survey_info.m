%global glbSurveyResponses glbSurveyHeaders;
global SURVEY_ANSWERS_LOCATION glbVideoInfos glbSurveyHeader glbSurveyRawResponses;

glbSurveyHeader = {};
glbSurveyRawResponses = {};

for i = 2:size(glbVideoInfos, 1)
	curID = glbVideoInfos{i, IDX_VIDEO_STR_ID};
	curSurveyResponseFile = fullfile(SURVEY_ANSWERS_LOCATION, [curID '_first.csv']);
	[subjectResponsesCell, headerCell] = ReadSurveyOutputFile(curSurveyResponseFile);
	%append a column to indicate which video was shown first
	headerCell(:,end+1) = {'Video Shown First'}; %#ok<SAGROW>
	subjectResponsesCell(:,end+1) = {curID}; %#ok<SAGROW>
	
	%verify all headers are identical
	if isempty(glbSurveyHeader)
		glbSurveyHeader = headerCell;
	else
		assert(isequal(glbSurveyHeader, headerCell));
	end
	
	glbSurveyRawResponses = [glbSurveyRawResponses; subjectResponsesCell(:,:)]; %#ok<AGROW>
end

clear curID curSurveyResponseFile subjectResponsesCell headerCell;

return