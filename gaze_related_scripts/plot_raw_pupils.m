close all;
PLOTS_PER_FIG = 4;
for vid_i = 2:size(glb.rawGazeSampleCell, 1)
	curVidName = glb.rawGazeSampleCell{vid_i, 1};
	curVidSubjNames = glb.rawGazeSampleCell{vid_i, 3};
	curVidRawSamples = glb.rawGazeSampleCell{vid_i, 4};
	curLenMS = glb.rawGazeSampleCell{vid_i, 2};
	time = [0:1:curLenMS-1];
	
	
	%extract the actual gaze samples into a matrix format suitable for PCA
	curNumSubjects = size(glb.rawGazeSampleCell{vid_i, 3}, 1);
	lastMaxLegend = 0;
	for subj_j = 1:curNumSubjects
		if (mod(subj_j-1, PLOTS_PER_FIG) == 0)
			figure; hold on; curPlotNum = 0;
			
		end
		plot(time, curVidRawSamples{subj_j}(1:curLenMS));
		if (mod(subj_j, PLOTS_PER_FIG) == 0) || (subj_j == curNumSubjects)
			legend(curVidSubjNames(lastMaxLegend+1:subj_j));
			title(sprintf('Raw pupil data for %s subj %d - %d', curVidName, lastMaxLegend+1, subj_j));
			lastMaxLegend = subj_j;	
		end
		%myMeans(subj_i) rawGazeSampleCell{vid_i, 5}(subj_j, :) = rawGazeSampleCell{vid_i, 4}{subj_j}(1:curVidNumSamples)';
	end
end