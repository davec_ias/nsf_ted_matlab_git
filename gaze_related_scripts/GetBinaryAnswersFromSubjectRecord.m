function [binaryAnswers] = GetBinaryAnswersFromSubjectRecord(aRecord)
NUM_ANSWERS = 42;
ANS_TO_Q = [...
	16, 16, 16, 16, 16, 16, 16, ...
	17, 17, 17, 17, 17, 17, 17, ...
	18, 18, 18, 18, 18, 18, 18, ...
	25, 25, 25, 25, 25, 25, 25, ...
	26, 26, 26, 26, 26, 26, 26, ...
	27, 27, 27, 27, 27, 27, 27 ...
];
	
ANSWERS = { ...
	'A. You', ... //1
	'B. You', ... //2
	'C. You', ... //3
	'D. You', ... //4
	'E. You', ... //5
	'F. You', ... //6
	'G. You', ... //7
...
	'A. Praise', ... //8
	'B. Praise', ... //9
	'C. Praise', ... //10
	'D. Praise', ... //11
	'E. Praise', ... //12
	'F. Praise', ... //13
	'G. Praise', ... //14
...
	'A. ...', ... //15
	'B. ...', ... //16
	'C. ...', ... //17
	'D. ...', ... //18
	'E. ...', ... //19
	'F. ...', ... //20
	'G. ...', ... //21
...
	'a. To', ... //22
	'b. To', ... //23
	'c. To', ... //24
	'd. To', ... //25
	'e. To', ... //26
	'f. To', ... //27
	'g. To', ... //28
...
	'a. Mil', ... //29
	'b. Bus', ... //30
	'c. The', ... //31
	'd. Bet', ... //32
	'e. Sol', ... //33
	'f. We ', ... //34
	'g. The', ... //35
...
	'a. Emp', ... //36
	'b. Lea', ... //37
	'c. Lea', ... //38
	'd. Upp', ... //39
	'e. Coo', ... //40
	'f. Emp', ... //41
	'g. Emp' ... //42
};
	
	
	

binaryAnswers = logical(NUM_ANSWERS);

for i = 1:NUM_ANSWERS
	colToUse = ANS_TO_Q(i);
	binaryAnswers(i) = ~isempty(strfind(aRecord{colToUse}, ANSWERS{i}));
end