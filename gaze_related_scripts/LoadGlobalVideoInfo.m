function LoadGlobalVideoInfo()
global VIDEO_INFOS, IDX_VIDEO_STR_ID, IDX_VIDEO_FILENAME, IDX_VIDEO_READER, IDX_VIDEO_SHOT_ANNO, IDX_VIDEO_GESTURE_ANNO;
assert(~isempty(VIDEO_INFOS));

global	IDX_VIDEO_STR_ID; 
		IDX_VIDEO_STR_ID = 1;
global	IDX_VIDEO_FILENAME; 
		IDX_VIDEO_FILENAME = 2;
global	IDX_VIDEO_READER; 
		IDX_VIDEO_READER = 3;
global	IDX_VIDEO_SHOT_ANNO; 
		IDX_VIDEO_SHOT_ANNO = 4;
global	IDX_VIDEO_GESTURE_ANNO; 
		IDX_VIDEO_GESTURE_ANNO = 5;

for i = 2:size(VIDEO_INFOS, 1)
	curVideoFileName = VIDEO_INFOS{i, IDX_VIDEO_FILENAME};
	curReader = VideoReader(curVideoFileName);
	
	if ((curReader.Height ~= ORIGINAL_VIDEO_HEIGHT_PX) || (curReader.Width ~= ORIGINAL_VIDEO_WIDTH_PX))
		warning('Video %s has unexpected dim (%d x %d), expected (%d x %d)', ...
				curFilecurVideoFileNameName, curReader.Height, curReader.Width, ORIGINAL_VIDEO_HEIGHT_PX, ORIGINAL_VIDEO_WIDTH_PX);
	end
end