function [gestureCell] = ReadGestureAnnotationFile(aFilename)
%output files for gesture annotation seem to be tab delimited and broken
%into two halfs. Each gesture has one line in each half. first half of file
%tells us what type of gesture, second half gives any extra annotated notes
%line format is always:
%TOOL_ID BLANK START_TIME_MS END_TIME_MS DURATION_MS TEXT_INFO
%where TOOL_ID is gesture_general4 in 1st half, gesture_description in 2nd
%BLANK is literally blank, nothing between the tabs
%next three are self explanatory
%TEXT_INFO is deictics, metaphoric, beats, or iconic in 1st half
%in 2nd half, TEXT info is whatever was typed in for that gesture as a free
%hand description. Doesn't ever seem to be blank
%note that cols 2-5 should be identical in the 1st and 2nd half


GESTURE_FILE_SPEC = '%q%q%u%u%u%q\n';
fileID = fopen(aFilename, 'r');
cleaner = onCleanup(@() fclose(fileID));
if (fileID < 0)
	error('Unable to open file for reading "%s"', aFilename);
end
results = textscan(fileID, GESTURE_FILE_SPEC, 'Delimiter','\t');
numLines = size(results{1}, 1);
if (mod(numLines, 2) ~= 0)
	error('File "%s" has odd number of lines - can'' handle this in ReadGestureAnnotationFile()', aFilename);
end
numGestures = numLines / 2;
gestureCell = cell(1, 5);
gestureCell(1) = {results{3}(1:numGestures)};
gestureCell(2) = {results{4}(1:numGestures)};
gestureCell(3) = {results{5}(1:numGestures)};
gestureCell(4) = {results{6}(1:numGestures)};
gestureCell(5) = {results{6}(numGestures+1:end)};

