%2016-09-07 davec@cs.columbia.edu
%to run, change the ROOT location in load_consts to a directory containing
%fixation reports. It will process any and all files in that directory 
%with "*.xls" extension 

%clearvars; clearvars -global;
load_consts;
load_video_info;
load_survey_info;
load_all_valid_subjects;

global glb;
[glb.surveyStats, glb.surveyOutput] = GetSurveyStats();
glb.rawGazeSampleCell = GetRawSamples();
