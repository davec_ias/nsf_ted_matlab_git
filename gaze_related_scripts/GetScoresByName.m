function [totalScore, carolScore, simonScore] = GetScoresByName(aSurveyStats, aSurveyOutput, aInitials)

index = GetSubjectIndexByInitials(aSurveyOutput, aInitials);
assert(index>0);
totalScore = aSurveyStats.numCorrectBySubj(index);
carolScore = aSurveyStats.numCorrectBySubjCarolOnly(index);
simonScore = aSurveyStats.numCorrectBySubjSimonOnly(index);