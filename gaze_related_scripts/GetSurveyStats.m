function [surveyStats, surveyOutput] = GetSurveyStats()

global GLOBALS;

surveyOutput = ReadAllSurveyOutput();
numSubjects = size(surveyOutput, 1);

surveyStats.binaryAnswers = logical(zeros(numSubjects, GLOBALS.SURVEY.NUM_ANSWERS)); %#ok<LOGL> %matlab 2011 doesn't support direct declaration of logicals...
surveyStats.binaryCorrectness = logical(zeros(numSubjects, GLOBALS.SURVEY.NUM_ANSWERS)); %#ok<LOGL>

for subj_i = 1:numSubjects
    surveyStats.binaryAnswers(subj_i, :) = GetBinaryAnswersFromSubjectRecord(surveyOutput(subj_i,:));
    surveyStats.binaryCorrectness(subj_i, :) = (surveyStats.binaryAnswers(subj_i, :) == GLOBALS.SURVEY.CORRECT_ANSWERS);
end

surveyStats.numCorrectBySubj = sum(surveyStats.binaryCorrectness, 2);
surveyStats.numCorrectByAnswer = sum(surveyStats.binaryCorrectness, 1);

surveyStats.numCorrectBySubjCarolOnly = sum(surveyStats.binaryCorrectness(:, 1:GLOBALS.SURVEY.NUM_CAROL_QUESTIONS), 2);
surveyStats.numCorrectBySubjSimonOnly = sum(surveyStats.binaryCorrectness(:, GLOBALS.SURVEY.NUM_CAROL_QUESTIONS+1:end), 2);

[surveyStats.coeffAnswers, surveyStats.scoreAnswers, surveyStats.latentAnswers] = princomp(double(surveyStats.binaryAnswers));
[surveyStats.coeffSubj, surveyStats.scoreSubj, surveyStats.latentSubj] = princomp(double(surveyStats.binaryAnswers)');

surveyStats.minByAnswer = min(surveyStats.numCorrectByAnswer);
surveyStats.maxByAnswer = max(surveyStats.numCorrectByAnswer);
surveyStats.medianByAnswer = median(surveyStats.numCorrectByAnswer);
surveyStats.meanByAnswer = mean(surveyStats.numCorrectByAnswer);

surveyStats.minBySubj= min(surveyStats.numCorrectBySubj);
surveyStats.maxBySubj = max(surveyStats.numCorrectBySubj);
surveyStats.medianBySubj = median(surveyStats.numCorrectBySubj);
surveyStats.meanBySubj = mean(surveyStats.numCorrectBySubj);
%hist(numCorrectByAnswer, [0:21]);

return