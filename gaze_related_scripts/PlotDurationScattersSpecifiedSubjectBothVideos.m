function PlotDurationScattersSpecifiedSubjectBothVideos(aSubjectsToProcess, aNumRowsPerFig)
DEFAULT_NUM_FIG_COLS = 4;
NUM_FIG_COLS = 2;
if ~exist('aNumRowsPerFig', 'var'), aNumRowsPerFig = DEFAULT_NUM_FIG_COLS; end
xLims = [0 720];
yLims = [0 5500];


curRow = aNumRowsPerFig + 1;
for i = 1:numel(aSubjectsToProcess)
	curID = aSubjectsToProcess{i};
	curData = GetValidSubjectData(curID);
	assert(~isempty(curData));	
	curGender = curData{2};
	curAge = curData{3};
	%curShownFirst = curData{4};
	
	carolFixationData = curData(5:9);
	simonFixationData = curData(10:14);
	
	if curRow > aNumRowsPerFig
		figure('position', [0, 0, 1600, 800]);
		curRow = 1;
	end
	subplot(aNumRowsPerFig, NUM_FIG_COLS, ((curRow-1)*NUM_FIG_COLS) + 1);
	PlotDurationsOneSubjectWholeVideo(carolFixationData, 'Carol', curID, curGender, curAge, xLims, yLims);
	
	subplot(aNumRowsPerFig, NUM_FIG_COLS, ((curRow-1)*NUM_FIG_COLS) + 2);
	PlotDurationsOneSubjectWholeVideo(simonFixationData, 'Simon', curID, curGender, curAge, xLims, yLims);
	curRow = curRow + 1;
end
