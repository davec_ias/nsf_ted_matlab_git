FORCE_RELOAD = false;
if ((~(exist('glbValidSubjects', 'var') == 1)) || FORCE_RELOAD)
	load_data_from_disk;
end

subjectsToProcess = {'sa1'; 'rn1'; 'an1'; 'sw1'}; %if you want all, subjects, use: subjectsToProcess = glbValidSubjects(2:end, 1);

PlotDurationScattersSpecifiedSubjectBothVideos(subjectsToProcess);

startTimeSec = 5.0;
stopTimeSec = 10.0;
figure;
OverlayFixationsOnVideo(SIMON_IDX, startTimeSec, stopTimeSec, subjectsToProcess);

startTimeSec = 340.0;
stopTimeSec = 360.0;
figure;
OverlayFixationsOnVideo(SIMON_IDX, startTimeSec, stopTimeSec, subjectsToProcess);
