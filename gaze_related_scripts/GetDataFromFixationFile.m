%2016-09-07 davec@cs.columbia.edu
%given a fixation report from the gaze software, returns a subjectData matrix of 
%fixations, with 5 columns and as many rows as the file contains fixations
%[FixationStartInMS FixationDurationInMS FixationEndInMS XPosInPixels YPosInPixels]
%also returns some metadata (mean, std dev, count, sum)

function [subjectData, meanDurationMS, stdDevDurationMS, numFixationsMS, ... 
		timeSpentFixatingMS] = GetDataFromFixationFile(aFilePath)

subjectData = [];
fid = fopen(aFilePath);
headerLine = fgetl(fid);
assert(ischar(headerLine), 'Malformed fixation file at %s', aFilePath);


timeCol = -1; durationCol = -1; xCol = -1; yCol = -1;

%The "xls" files from the gaze software are actually just tab delimited
%text files with sloppy file extension
colNames = strsplit(headerLine, '\t');
numCols = numel(colNames);

%We must be tolerant of different column orders as well as missing columns
%so parse header line for the cols we care about
for i = 1:numCols
	curColName = colNames{i};
	if strcmp(curColName, 'CURRENT_FIX_START')
		timeCol = i;
	elseif strcmp(curColName, 'CURRENT_FIX_DURATION')
		durationCol = i;
	elseif strcmp(curColName, 'CURRENT_FIX_X')
		xCol = i;
	elseif strcmp(curColName, 'CURRENT_FIX_Y')
		yCol = i;
	end
end



%while using textscan() would be more efficient, it seems to choke on
%either the EOL conventions of presence of literal strings '[]' in the
%content, and I have no time to keep futzing with it - davec
while (true)
	curLine = fgetl(fid);	
	if ~ischar(curLine), break; end
	colVals = strsplit(curLine, '\t');
	%format is [startTime durationMS endTime x y]. note endTime is
	%constructed from first two cols, and is thus for convenience
	curLineData = [str2double(colVals(timeCol)) str2double(colVals(durationCol)) (str2double(colVals(timeCol)) + str2double(colVals(durationCol))) str2double(colVals(xCol)) str2double(colVals(yCol))];
	subjectData = [subjectData ; curLineData]; %#ok<AGROW>
end
fclose(fid);


%sanity checks on data
%all durations positive
assert(all((subjectData(:, 2) > 0)));
%first start time non neg, each start time is later than previous
assert((subjectData(1, 1) >= 0) && all(subjectData(2:end, 1) > subjectData(1:end-1,1)));
%each start time later than previous end time
assert(all(subjectData(2:end, 1) > (subjectData(1:end-1,3))));

meanDurationMS = mean(subjectData(:, 2));
stdDevDurationMS = std(subjectData(:, 2));
numFixationsMS = numel(subjectData(:, 2));
timeSpentFixatingMS = sum(subjectData(:, 2));