X_DUR_TO_SHOW = 15;
Y_DUR_TO_SHOW = 2;
ROWS_PER_FIG = 4;
COLS_PER_FIG = 4;
FIGURE_POSITION =  [100, 100, 1700, 900];
SCATTER_SIZE = 10;

frameNumsOmitFinal = shotStartFrameNums(1:end-1);
frameIDsOmitFinal = shotTypesIDNums(1:end-1);

totalNumFix = 0;
allIndicesUsed = logical(zeros(size(allStarts))); %#ok<LOGL>

global NUM_SHOT_TYPES;

for i_shotType = 0:NUM_SHOT_TYPES-1
	curShotType = ShotTypeIDToDescriptionString(i_shotType);
	curSubPlotNum = (ROWS_PER_FIG * COLS_PER_FIG) + 1;
	curShotTypeMatchingIndices = frameIDsOmitFinal == i_shotType;
	startFrames = frameNumsOmitFinal(curShotTypeMatchingIndices);
	endFrames = shotStartFrameNums([false; curShotTypeMatchingIndices]);
	for j_frame = 1:numel(startFrames)
		if j_frame == numel(startFrames) && (i_shotType == 6)
			breakP = 1;
		end
		if curSubPlotNum > (ROWS_PER_FIG * COLS_PER_FIG)
			figure('Position', FIGURE_POSITION); set(gcf,'color','w');
			curSubPlotNum = 1;
		end
		
		curStartTimeSecs = double(startFrames(j_frame)) / FPS_OLD; 
		curEndTimeSecs = double(endFrames(j_frame)) / FPS_OLD;
		matchingIndices = (allStarts >= curStartTimeSecs * 1000) & (allStarts < curEndTimeSecs * 1000);
		allIndicesUsed = allIndicesUsed | matchingIndices;
		matchingStartsMS = allStarts(matchingIndices);
		matchingDurationsMS = allDurations(matchingIndices);

		%matlab indexes subplot left to right, we want top to bottom...
		desiredCol = ceil(curSubPlotNum / ROWS_PER_FIG);
		desiredRow = curSubPlotNum - ((desiredCol-1) * ROWS_PER_FIG);
		translatedSubPlotNum = ((desiredRow - 1) * COLS_PER_FIG) + desiredCol;
		
		if (curSubPlotNum == 7)
			breakP = 1;
		end
		
		subplot(ROWS_PER_FIG, COLS_PER_FIG, translatedSubPlotNum);
		scatter((matchingStartsMS ./ 1000) - curStartTimeSecs, matchingDurationsMS ./ 1000, SCATTER_SIZE, '.');
		xlim([0 X_DUR_TO_SHOW]);
		ylim([0 Y_DUR_TO_SHOW]);
		title(sprintf('%s - %s (%4.1f - %4.1f)', curShotType, VIDEO_TO_USE_OLD, curStartTimeSecs, curEndTimeSecs));
		curSubPlotNum = curSubPlotNum + 1;
		totalNumFix = totalNumFix + numel(matchingStartsMS);
	end
end

allThatShouldHaveBeenDisplayed = allStarts(allStarts < ((shotStartFrameNums(end) / FPS_OLD) * 1000));
if ~all(totalNumFix == numel(allThatShouldHaveBeenDisplayed))
	warning('TODO - debug unexpected number of fixations here - suspect annotation has duplicate/overlapping shots, e.g. multiple shots (62 and 63) noted as start frame [15081] in Carol''s data. Should impose sanity checks on annoations');
end
