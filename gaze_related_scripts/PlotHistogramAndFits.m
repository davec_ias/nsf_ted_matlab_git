function PlotHistogramAndFits(aHistogramData, aRawData, aYLim, aUsePoisson, aUseNormal, aUseLogNormal, aBinLabels)

if ~exist('aUsePoisson', 'var'), aUsePoisson = true; end
if ~exist('aUseNormal', 'var'), aUseNormal = true; end
if ~exist('aUseLogNormal', 'var'), aUseLogNormal = false; end
if ~exist('aYScalar', 'var'), aYScalar = 1; end
if ~exist('aBinLabels', 'var'), aBinLabels = [0:size(aHistogramData, 2)-1]; end

bar(aBinLabels, aHistogramData);

xlim([0 max(aBinLabels)]); 
if (exist('aYLim', 'var') && aYLim(1) >= 0), ylim(aYLim); end

hold on;
if aUsePoisson
	pd = fitdist(aRawData, 'Poisson');
	y = pdf(pd, aBinLabels); 
	plot(aBinLabels,y,'LineWidth',1, 'Color', 'Red');
end

if aUseNormal
	pd = fitdist(aRawData, 'Normal');
	y = pdf(pd, aBinLabels);
	plot(aBinLabels,y,'LineWidth',1, 'Color', 'Blue');
end

if aUseLogNormal
	pd = fitdist(aRawData, 'LogNormal');
	y = pdf(pd, aBinLabels);
	plot(aBinLabels,y,'LineWidth',1, 'Color', 'Magenta');
end

hold off;