function [subjectIndex] = GetSubjectIndexByInitials(aSurveyOutput, aInitials)

for subjectIndex = 1:size(aSurveyOutput, 1)
	if strcmpi(aInitials, aSurveyOutput{subjectIndex, 1}), return; end
end
warning('Could not find subject %s in surveyOutput', aInitials);
subjectIndex = -1;