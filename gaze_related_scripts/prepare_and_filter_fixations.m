%2016-09-07 davec@cs.columbia.edu

allFixations = [];
for i = 1:numSubj
	fprintf('Preparing and filtering subject %d\n', i);
	
	curData = FilterData(subjectData{i}, FIXATION_DURATION_MIN_THRESHOLD_MS, ...
			FIXATION_DURATION_MAX_THRESHOLD_MS, FIXATION_MIN_X, FIXATION_MIN_Y, FIXATION_MAX_X, FIXATION_MAX_Y);
		
	allFixations = [allFixations; curData(:,[1 3])]; %#ok<AGROW>
end

allStarts = allFixations(:,1);
allEnds = allFixations(:,2);
allDurations = allEnds - allStarts;
changeTimes = sortrows(unique([allStarts; allEnds]));