function PlotDurationsOneSubjectWholeVideo(aFixationData, aVideoID, aSubjectID, aGender, aAge, xLims, yLims);
SCATTER_DOT_SIZE = 10;
fixations = aFixationData{1};

subjDescrString = sprintf('"%s" watching %s. (%s aged %d)', aSubjectID, aVideoID, aGender, aAge);
statsStr = sprintf('%4d Fixations. Duration (ms): mean %4.0f stdDev %4.0f sum %d', ...
	 aFixationData{4}, aFixationData{2}, aFixationData{3}, aFixationData{5});
	
scatter(fixations(:, 1)/1000, min(fixations(:, 2), yLims(2) - 500), SCATTER_DOT_SIZE, '.');
title(sprintf('%s\n%s', subjDescrString, statsStr));
xlim(xLims); ylim(yLims);
	
