%2016-09-07 davec@cs.columbia.edu

%Compute a running total of how many subjects are fixating at any given
%moment. One thing to keep in mind to understand this code is that the 
%total can only change at the moment a fixation (from any of the
%subjects) starts or ends. The total will stay the same at all other times.

%simple brute force computation based on the observation that for each
%possible change time, the number of people with their eyes open is equal
%to the number of fixations starts that have occured prior to that time,
%minue the number of fixation ends that have occured prior to that time.
%we then loops once through the list of possible change times, performing
%this arithmetic at each point. O(n*m) where n is change points and m is
%total number of fixations among all subjects. Could easily be done in
%something better like O(n*S), where S is a small const, likely the number 
%of human subjects
%todo more efficent computation using only vector ops. alternately,
%implement via one pass iteration and no vector ops. Either would be faster
activeSubjectCounts = zeros(numel(changeTimes)+1, 3);
activeSubjectCounts(2:end, 1) = changeTimes;
for i = 1:numel(changeTimes)+1
	activeSubjectCounts(i, 2) = sum(allStarts <= activeSubjectCounts(i,1)) - sum(allEnds <= activeSubjectCounts(i,1));
	if (mod(i,5000) == 0), fprintf('Processed row %d of %d\n', i, numel(changeTimes)+1); end
end

%each count needs to be associated with a duration for weighting purposes
%while we average over time later
activeSubjectCounts(:, 3) = [(activeSubjectCounts(2:end,1) - activeSubjectCounts(1:end-1,1)); 0];