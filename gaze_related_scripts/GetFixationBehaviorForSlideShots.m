%function [slidesTotalDuration, slideFixationCounts, slideMeanFixationDurations, slideTotalFixationDurations, slideFixationLists, slidePercentsFixated...
%		otherTotalDuration, otherFixationCounts, otherMeanFixationDurations, otherTotalFixationDurations, otherFixationLists, otherPercentsFixated] = ...
		
function [fixationBehavior] = ...
	GetFixationBehaviorForSlideShots(aVideoInfo, aSubjectFixations)

global IDX_VIDEO_SHOT_ANNO IDX_VIDEO_FPS IDX_VIDEO_READER;

FPS = aVideoInfo{IDX_VIDEO_FPS};
shotAnnotations = aVideoInfo{IDX_VIDEO_SHOT_ANNO};
shotStartTimes = shotAnnotations{1} / FPS * 1000; 
shotIDs = shotAnnotations{2};
endTimeMS = aVideoInfo{IDX_VIDEO_READER}.Duration * 1000;

fixationBehavior = {
	'FixationType', 'Counts', 'TotalDurations', 'TimesSpentFixating', 'MeanFixationDurations', 'PercentsFixated', 'RawFixations';
	'Slides'      ,       [],               [],                   [],                      [],                [],              {};
	'Other'       ,       [],               [],                   [],                      [],                [],              {};
};

SLIDES_ID = 4; %todo lookup from consts
numSubjects = size(aSubjectFixations, 1);
numShots = size(shotAnnotations{1}, 1);
numSlideShots = sum(shotAnnotations{1,2} == SLIDES_ID);
numOtherShots = numShots - numSlideShots;


for fixationType_i = 2:3
	fixationBehavior{fixationType_i, 2} = zeros(numSubjects, 1);
	fixationBehavior{fixationType_i, 3} = zeros(numSubjects, 1);
	fixationBehavior{fixationType_i, 4} = zeros(numSubjects, 1);
	fixationBehavior{fixationType_i, 5} = zeros(numSubjects, 1);
	fixationBehavior{fixationType_i, 6} = zeros(numSubjects, 1);
	fixationBehavior{fixationType_i, 7} = cell(numSubjects, 1);
end

for subj_i = 1:numSubjects
	curSubjFixations = aSubjectFixations{subj_i}; 
	for shot_j = 1:numShots
		curShotStartMS = shotStartTimes(shot_j); 
		if shot_j < numShots
			curShotEndMS = shotStartTimes(shot_j+1); 
		else
			curShotEndMS = endTimeMS; 
		end
		curShotTypeID = shotIDs(shot_j);
		if (curShotTypeID == SLIDES_ID)
			rowToInc = 2;
		else
			rowToInc = 3;
		end
		
		matchingFixationIdxs = (curSubjFixations(:, 1) >= curShotStartMS) & (curSubjFixations(:, 1) < curShotEndMS);
		curMatchingFixations = curSubjFixations(matchingFixationIdxs, :);
		
		fixationBehavior{rowToInc, 2}(subj_i) = fixationBehavior{rowToInc, 2}(subj_i) + sum(matchingFixationIdxs);
		fixationBehavior{rowToInc, 3}(subj_i) = fixationBehavior{rowToInc, 3}(subj_i) + (curShotEndMS - curShotStartMS);
		fixationBehavior{rowToInc, 4}(subj_i) = fixationBehavior{rowToInc, 4}(subj_i) + sum(curMatchingFixations(:, 2));
		fixationBehavior{rowToInc, 7}{subj_i} = [fixationBehavior{rowToInc, 7}{subj_i}; curMatchingFixations];

	end
	fixationBehavior{2, 5}(subj_i) = fixationBehavior{2, 4}(subj_i) / fixationBehavior{2, 2}(subj_i);
	fixationBehavior{2, 6}(subj_i) = fixationBehavior{2, 4}(subj_i) / fixationBehavior{2, 3}(subj_i);
	
	fixationBehavior{3, 5}(subj_i) = fixationBehavior{3, 4}(subj_i) / fixationBehavior{3, 2}(subj_i);
	fixationBehavior{3, 6}(subj_i) = fixationBehavior{3, 4}(subj_i) / fixationBehavior{3, 3}(subj_i);
end