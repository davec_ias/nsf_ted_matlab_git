Y_LIM = [0 .25];
NUM_FIGURE_ROW = 5;
NUM_FIGURE_COL = 3;
NUM_PLOT_PER_FIG = NUM_FIGURE_ROW * NUM_FIGURE_COL;

binLabels = 0:numBins-1;

for i_subj = 1:numSubj
	curSubjData = fixationCountHistBySubject(i_subj, :);
	
	curSubPlotNum = mod(i_subj, NUM_PLOT_PER_FIG);
	if (curSubPlotNum == 0), curSubPlotNum = NUM_PLOT_PER_FIG; end
	if (curSubPlotNum == 1), figure; set(gcf,'color','w'); end
	subplot(NUM_FIGURE_ROW, NUM_FIGURE_COL, curSubPlotNum);
	
	PlotHistogramAndFits(curSubjData, windowedFixationCountsBySubject(i_subj, :)', Y_LIM);
	if (i_subj == 3), ylim(Y_LIM .* 2); end %quick fix for bad data
	title(sprintf('Subj. %d, %ds win', i_subj, WINDOW_WIDTH_MS_WINDOWED_COUNTS / 1000));
end

figure; set(gcf,'color','w');
PlotHistogramAndFits(sum(fixationCountHistBySubject) / numSubj, windowedFixationCountsBySubject(:), Y_LIM);
title(sprintf('Histogram of # of fixations per window\n(Avg. over all subjects, windows with %ds sec width and %ds shift)', ...
		WINDOW_WIDTH_MS_WINDOWED_COUNTS / 1000, WINDOW_SHIFT_MS_WINDOWED_COUNTS / 1000));
legend('Actual histogram', 'Best Fit Poisson', 'Best Fit Normal');



