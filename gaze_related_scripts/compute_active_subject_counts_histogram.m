%"+ 1" in places due to need to adjust for matlab one-based array index 

%holds a "flattened" version of our counts (e.g. instead of duration and/or
%end columns, just have one column per millisecond). suitable for use with
%matlab's fitter, or histogram function
activeSubjectCountsByMS = zeros(sum(activeSubjectCounts(:,3)) + 1, 1);

%one bin for each possible number of subjects, incl. zero
histCounts = zeros(1, numSubj+1);
histLabels = 0:numSubj;

numPoints = size(activeSubjectCounts, 1);
for i = 1:numPoints
	curStartTime = activeSubjectCounts(i, 1);
	curCount = activeSubjectCounts(i, 2); 
	curDurationMS = activeSubjectCounts(i, 3);
	
	curEndTime = curStartTime + curDurationMS;
	activeSubjectCountsByMS(curStartTime+1:curEndTime) = curCount;
	
	histCounts(curCount + 1) = histCounts(curCount + 1) + curDurationMS;
end

assert(endOfVideoMS == sum(histCounts));
histCounts = histCounts ./ sum(histCounts); %normalize


return

