SOURCE_DIR = 'C:\nsf_ted_matlab_data\video_shot_annotations';

filenames = {
	'simon_sinek_why_good_leaders_make_you_feel_safe.20160330_024328.annotation';
	'carol_dweck_the_power_of_believing_that_you_can_improve.20160330_021338.annotation';
	'kelly_mcgonigal_how_to_make_stress_your_friend.20160330_030621.annotation';
	'malcolm_gladwell_the_unheard_story_of_david_and_goliath.20160330_034934.annotation'
};

for file_i = 1:4
	fullpath = fullfile(SOURCE_DIR, filenames{file_i});
	underscoreIndices = strfind(filenames{file_i}, '_');
	easyName = filenames{file_i}(1:underscoreIndices(2)-1);
	statResults = get_annotation_stats(fullpath);
	statResults.displayName = easyName;
	printStatResults(statResults);
end